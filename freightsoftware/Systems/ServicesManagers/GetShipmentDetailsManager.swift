//
//  GetShipmentDetailsManager.swift
//  freightsoftware
//
//  Created by AB on 18/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import ObjectMapper


typealias GetShipmentDetailsResponse = (_ search: ShipmentItemDetailsClass?) -> ()

/**
 *  This struct helps in Product relates CRUD operations. Mainly interacts with Network Resources.
 */
struct  GetShipmentDetailsManager {
    
    /**
     Lists products from Server DB.
     
     - parameter completion: returns `ProductResponse` which is having products' array if success otherwise `nil`
     */
    
    func getShipmentDetails(param: [String : AnyObject],completion: @escaping GetShipmentDetailsResponse) {
        // let URLstr = "\(baseUserUrl)"+"driverLogin"
        //let URLString = URLstr.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        //print(URLstr)
        print(aBasePostGetShipmentDetailsURl)
        print(param)
        
        
        //Alamofire.request(URLstr, parameters: nil, encoding: URLEncoding.default)
        
        Alamofire.request(aBasePostGetShipmentDetailsURl, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: authHeader())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/plain"])
            .responseSwiftyJSON { response in
                // debugPrint(response.request!.URL?.absoluteString.stringByRemovingPercentEncoding)
                print(response)
                
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                
                //print(result)
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<ShipmentItemDetailsClass>().map(JSONString: result)
                        //let resultData = Mapper<ClockInOutClass>().mapArray(JSONString: result)
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    
                    //  Maps JSON Array of products into Product Object's Array
                    //print(resultData)
                    completion(resultData)
                    return
                }
                
                //popup message no more data found
        }
        
    }
    
    private func authHeader() -> [String : String] {
        return [
            // "X-Authorization": "\(kBaseApiKey)",
            "Content-Type": "application/x-www-form-urlencoded",
            //"Accept-Language":"\(language)"
        ]
    }
    
}

