//
//  DimListTableViewCell.swift
//  freightsoftware
//
//  Created by DK on 21/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class DimListTableViewCell: UITableViewCell {
    
    
    //#MARK:- Outlets
    
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet var lblQuantityValue: UILabel!
    @IBOutlet var lblStatedWeight: UILabel!
    @IBOutlet var lblStatedWeightValue: UILabel!
    @IBOutlet var lblLenght: UILabel!
    @IBOutlet var lblLenghtValue: UILabel!
    @IBOutlet var lblWidth: UILabel!
    @IBOutlet var lblWidthValue: UILabel!
    @IBOutlet var lblHeight: UILabel!
    @IBOutlet var lblHeightValue: UILabel!
    @IBOutlet var lblDim: UILabel!
    @IBOutlet var lblDimValue: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblDescriptionValue: UILabel!    
    @IBOutlet var btnDelete: xUIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblQuantity.text = mapping.string(forKey: "Quantity_key")
        lblStatedWeight.text = mapping.string(forKey: "StatedWeight_key")
        lblLenght.text = mapping.string(forKey: "Length_key")
        lblWidth.text = mapping.string(forKey: "Width_key")
        lblHeight.text = mapping.string(forKey: "Height_key")
        lblDim.text = mapping.string(forKey: "Dim_key")
        lblDescription.text = mapping.string(forKey: "Description_key")
        btnDelete.setTitle(mapping.string(forKey: "DeleteCapital_key"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(theModel:FreightDetailsDimListClass)
    {
        lblQuantityValue.text! = "\(theModel.quantity!) "+"\(theModel.piecesType!)"
        lblStatedWeightValue.text! = "\(theModel.weight!)"
        lblLenghtValue.text! = "\(theModel.l!)"
        lblWidthValue.text! = "\(theModel.w!)"
        lblHeightValue.text! = "\(theModel.h!)"
        lblDimValue.text! = "\(theModel.dimFactor!)"
        lblDescriptionValue.text! = "\(theModel.pieceDescription!)"
        btnDelete.tag = self.tag-1
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        print(sender.tag)
        if self.viewNextPresentingViewController() != nil {
            
            if  let FreightDetailsVC =  self.viewNextPresentingViewController() as? FreightDetailsViewController {
                FreightDetailsVC.deleteDim(selectedId:sender.tag)
                
            }
        }
    }
}
