//
//  AddDimTableViewCell.swift
//  freightsoftware
//
//  Created by DK on 21/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import DropDown

class AddDimTableViewCell: UITableViewCell,UITextViewDelegate,UITextFieldDelegate {

    //#MARK:- Outlets
    @IBOutlet var lblPallets: UILabel!
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet var txtQuantitryValue: UITextField!
    @IBOutlet var imgHazmat: UIImageView!
    @IBOutlet var lblHazmat: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblTypeValue: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var txtWeightValue: UITextField!
    @IBOutlet var lblLength: UILabel!
    @IBOutlet var txtLenghtValue: UITextField!
    @IBOutlet var lblWidth: UILabel!
    @IBOutlet var txtWidthValue: UITextField!
    @IBOutlet var lblHeight: UILabel!
    @IBOutlet var txtHeightValue: UITextField!
    @IBOutlet var lblDim: UILabel!
    @IBOutlet var txtDimValue: UITextField!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var txtDescription: UITextView!
    @IBOutlet var btnAdd: xUIButton!
    
    //#MARK:- Variable Declare
    
    var pallets = mapping.string(forKey: "Pallets_PCS_key")
    var DimTypeDD = DropDown()
    var Enter_here_key = mapping.string(forKey: "Enter_here_key")
    var theDimTypeList = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblPallets.text = pallets
        lblQuantity.text = mapping.string(forKey: "QuantityCapital_key")
        txtQuantitryValue.placeholder = mapping.string(forKey: "Enter_here_key")
        lblHazmat.text = mapping.string(forKey: "Hazmat_key")
        lblType.text = mapping.string(forKey: "TypeCapital_key")
        lblTypeValue.text = mapping.string(forKey: "SelectHere_key")
        lblWeight.text = mapping.string(forKey: "WeightCapital_key")
        txtWeightValue.placeholder = mapping.string(forKey: "Enter_here_key")
        lblLength.text = mapping.string(forKey: "LengthCapital_key")
        txtLenghtValue.placeholder = mapping.string(forKey: "Enter_here_key")
        lblWidth.text = mapping.string(forKey: "WidthCapital_key")
        txtWidthValue.placeholder = mapping.string(forKey: "Enter_here_key")
        lblHeight.text = mapping.string(forKey: "HeightCapital_key")
        txtHeightValue.placeholder = mapping.string(forKey: "Enter_here_key")
        lblDim.text = mapping.string(forKey: "DimCapital_key")
        txtDimValue.placeholder = mapping.string(forKey: "Enter_here_key")
        lblDescription.text = mapping.string(forKey: "DescriptionCapital_key")
        txtDescription.text = Enter_here_key
        txtDescription.textColor = UIColor.lightGray
        btnAdd.setTitle(mapping.string(forKey: "AddCapital_key"), for: .normal)
        configDropdown(dropdown: DimTypeDD, sender: lblTypeValue)
        [txtQuantitryValue,txtWeightValue,txtLenghtValue,txtWidthValue,txtHeightValue,txtDimValue].forEach { (txt) in
            txt?.delegate = self
        }
        txtDescription.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    // MARK:- Other Methods
    
    @IBAction func btnShowHideHazmat(_ sender: UIButton) {
        if self.viewNextPresentingViewController() != nil {
            
            if  let FreightDetailsVC =  self.viewNextPresentingViewController() as? FreightDetailsViewController {
                if FreightDetailsVC.isHazmat
                {
                    FreightDetailsVC.isHazmat = false
                    imgHazmat.image = #imageLiteral(resourceName: "checkbox_unchecked")
                }else{
                    FreightDetailsVC.isHazmat = true
                    imgHazmat.image = #imageLiteral(resourceName: "checkbox_checked")
                }
                
            }
        }
    }
    
    @IBAction func btnDimTypeSelectAction(_ sender: UIButton) {
        if(self.theDimTypeList.count > 0)
        {
            [txtQuantitryValue,txtWeightValue,txtLenghtValue,txtWidthValue,txtHeightValue,txtDimValue].forEach { (txt) in
                txt?.resignFirstResponder()
            }
            showDropDown(theDimtypeList:self.theDimTypeList)
        }
    }
    
    @IBAction func btnAddAction(_ sender: UIButton) {
        if self.viewNextPresentingViewController() != nil {
            
            if  let FreightDetailsVC =  self.viewNextPresentingViewController() as? FreightDetailsViewController {
                FreightDetailsVC.validateAddDim()
               
            }
        }
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = kDefaultSeparationLineColorPrimary
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = Enter_here_key
            textView.textColor = UIColor.lightGray
        }else{
            if self.viewNextPresentingViewController() != nil {
                
                if  let FreightDetailsVC =  self.viewNextPresentingViewController() as? FreightDetailsViewController {
                    FreightDetailsVC.DimDescription = textView.text!
                    
                }
            }
            
        }
    }
    func showDropDown(theDimtypeList:[String]) {
        DimTypeDD.dataSource = theDimtypeList
        DimTypeDD.show()
        
        DimTypeDD.selectionAction = { (index,item) in
            self.lblTypeValue.text = item
            if self.viewNextPresentingViewController() != nil {
                
                if  let FreightDetailsVC =  self.viewNextPresentingViewController() as? FreightDetailsViewController {
                    FreightDetailsVC.DimTypeSelectedIndex = index
                    FreightDetailsVC.SelectedDimType = item
                    
                }
            }
        }
    }
    func hideDropDown() {
        DimTypeDD.hide()
    }
    // MARK:- Config DropDown
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    
    // MARK:- Set Data
    func setData(NoOfPallets:Int,SelectedDimType:String,DimQuantity:Int,DimWeight:Int,DimLenght:Int,DimWidth:Int,DimHeight:Int,DimValue:Int,DimDescription:String)
    {
        if(NoOfPallets > 0)
        {
            lblPallets.text = pallets!+" \(NoOfPallets)"
        }else{
            lblPallets.text = pallets
        }
        if(DimQuantity > 0)
        {
            txtQuantitryValue.text = "\(DimQuantity)"
        }else{
            txtQuantitryValue.text = ""
        }
        
        if(DimWeight > 0)
        {
            txtWeightValue.text = "\(DimWeight)"
        }else{
            txtWeightValue.text = ""
        }
        if(SelectedDimType != "")
        {
            lblTypeValue.text = "\(SelectedDimType)"
        }else{
             lblTypeValue.text = mapping.string(forKey: "SelectHere_key")
        }
        txtLenghtValue.text = "\(DimLenght)"
        txtWidthValue.text = "\(DimWidth)"
        txtHeightValue.text = "\(DimHeight)"
        txtDimValue.text = "\(DimValue)"
        
        if(DimDescription != "")
        {
            txtDescription.text = "\(SelectedDimType)"
        }else{
            txtDescription.text = Enter_here_key
        }
        
        if self.viewNextPresentingViewController() != nil {
            
            if  let FreightDetailsVC =  self.viewNextPresentingViewController() as? FreightDetailsViewController {
                if FreightDetailsVC.isHazmat
                {
                    imgHazmat.image = #imageLiteral(resourceName: "checkbox_checked")
                }else{
                    imgHazmat.image = #imageLiteral(resourceName: "checkbox_unchecked")
                }
                
            }
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.viewNextPresentingViewController() != nil {
            
            if  let FreightDetailsVC =  self.viewNextPresentingViewController() as? FreightDetailsViewController {
                if(textField.tag == 1)
                {
                    FreightDetailsVC.DimQuantity = Int(textField.text!)!
                }
                else if(textField.tag == 2)
                {
                    FreightDetailsVC.DimWeight = Int(textField.text!)!
                }
                else if(textField.tag == 3)
                {
                    FreightDetailsVC.DimLenght = Int(textField.text!)!
                }
                else if(textField.tag == 4)
                {
                    FreightDetailsVC.DimWidth = Int(textField.text!)!
                }
                else if(textField.tag == 5)
                {
                    FreightDetailsVC.DimHeight = Int(textField.text!)!
                }
                else if(textField.tag == 6)
                {
                    FreightDetailsVC.dimDim = Int(textField.text!)!
                }
                
            }
        }
    }
    
    
}
