//
//  HomeVCTableViewCell.swift
//  freightsoftware
//
//  Created by DK on 17/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class HomeVCTableViewCell: UITableViewCell {
    //#MARK:- Outlets
    
    @IBOutlet var lblControl: UILabel!
    @IBOutlet var lblControlValue: UILabel!
    @IBOutlet var lblMoveType: UILabel!
    @IBOutlet var lblMoveTypeValue: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblTypeValue: UILabel!
    @IBOutlet var lblDetails: UILabel!
    @IBOutlet var lblDetailsValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblControl.text = mapping.string(forKey: "Control_key")
        lblMoveType.text = mapping.string(forKey: "MoveType_key")
        lblType.text = mapping.string(forKey: "Type_key")
        lblDetails.text = mapping.string(forKey: "Details_key")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTheData(theModel: ShipmentListClass)
    {
        lblControlValue.text! = " # "+"\(theModel.clientCN!)"
        lblMoveTypeValue.text! = theModel.moveType!
        lblTypeValue.text! = theModel.shipmentType!
        lblDetailsValue.text! = theModel.freightDetails!
    }
    
}
