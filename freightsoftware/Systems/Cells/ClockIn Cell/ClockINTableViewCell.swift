//
//  ClockINTableViewCell.swift
//  freightsoftware
//
//  Created by DK on 17/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class ClockINTableViewCell: UITableViewCell {

    //#MARK:- Outlets
    
    @IBOutlet var lblCheckIn: UILabel!
    @IBOutlet var lblCheckInTime: UILabel!
    @IBOutlet var lblCheckInDate: UILabel!
    
    @IBOutlet var lblCheckOut: UILabel!
    @IBOutlet var lblcheckOutTime: UILabel!
    @IBOutlet var lblCheckOutDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblCheckIn.text = mapping.string(forKey: "ClockIn_key")
        lblCheckOut.text = mapping.string(forKey: "ClockOut_key")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTheData(theModel: ClockInOutClass)
    {
        if let Cin = theModel.clockIn
        {
            
            let myString = Cin.digits
            let result = String(myString.characters.filter { String($0).rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789")) != nil })
            print(result)
            let number = Int(result)
            if(number != nil)
            {
                let datee = Date(milliseconds: number!)
                print( DateToString(Formatter:"M/dd/yyyy",date:datee))
                self.lblCheckInDate.text! = DateToString(Formatter:"M/dd/yyyy",date:datee)
                self.lblCheckInTime.text! = DateToString(Formatter:"hh:mm:ss a",date:datee)
            }
            
                
            //let number = Int(Cin.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
            
            /*if let number = Int(result) {
                let datee = Date(milliseconds: number)
                print( DateToString(Formatter:"M/dd/yyyy",date:datee))
                self.lblCheckInDate.text! = DateToString(Formatter:"M/dd/yyyy",date:datee)
                self.lblCheckInTime.text! = DateToString(Formatter:"hh:mm:ss a",date:datee)
                
            }*/
        }else{
            self.lblCheckInDate.text! = ""
            self.lblCheckInTime.text! = ""
        }
        
        if let COut = theModel.clockOut
        {
            let myString = COut.digits
            let result = String(myString.characters.filter { String($0).rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789")) != nil })
            print(result)
            //let number = Int(Cin.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
            
             let number = Int(result)
            if(number != nil)
            {
                let datee = Date(milliseconds: number!)
                print( DateToString(Formatter:"M/dd/yyyy",date:datee))
                self.lblCheckOutDate.text! = DateToString(Formatter:"M/dd/yyyy",date:datee)
                self.lblcheckOutTime.text! = DateToString(Formatter:"hh:mm:ss a",date:datee)
            }
            
        }
        else{
            self.lblCheckOutDate.text! = ""
            self.lblcheckOutTime.text! = ""
        }
    }
    //MARK:- Date Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        // dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //   dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
}
extension String {
    private static var digits = UnicodeScalar("0")..."9"
    var digits: String {
        return String(unicodeScalars.filter{ String.digits ~= $0 })
    }
}
