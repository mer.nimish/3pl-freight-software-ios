//
//  ItemDetailsTableViewCell.swift
//  freightsoftware
//
//  Created by DK on 18/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class ItemDetailsTableViewCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
