//
//  SideMenuTableCell.swift
//  HappyLiving
//
//  Created by Haresh on 11/30/17.
//  Copyright © 2017 Haresh. All rights reserved.
//

import UIKit

class SideMenuTableCell: UITableViewCell {

    @IBOutlet var backView: UIView!
    @IBOutlet weak var imgSideMenu: UIImageView!
    
    @IBOutlet weak var lblSideMenu: UILabel!
    
    @IBOutlet weak var vwBlue: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
