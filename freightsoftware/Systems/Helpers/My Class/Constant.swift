//
//  Constant.swift
//  PizzyBites
//
//  Created by Abhay on 03/06/17.
//  Copyright © 2017 Abhay. All rights reserved.
//

import Foundation
import UIKit

let Screen = UIScreen.main.bounds
var ScreenHight = Screen.height
var ScreenWidth = Screen.width
var ScreenX = Screen.origin.x
var ScreenY = Screen.origin.y

import UIKit


extension UIColor {
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

let kDefaultPrimaryColor = UIColor(rgb: 0x70c3e6)
let kDefaultViewBackgroundColor = UIColor(rgb: 0xF4F4F4)
let kDefaultTextColor = UIColor(rgb: 0x0083bf)
let kDefaultDarkGrayColor = UIColor(rgb: 0xAAAAAA)
let kDefaultWhiteColor = UIColor(rgb: 0xFFFFFF)
let kDefaultBlackColor = UIColor(rgb: 0x000000)
let kDefaultSeparationLineColorPrimary = UIColor(rgb: 0xAAAAAA)
let kDefaultSeparationViewLineColorPrimary = UIColor(rgb: 0xdcdbdb)
let kDefaultSidemenuSelectedColor = UIColor(rgb: 0xD6EEF8)



let kDefaultViewColorPrimary = UIColor(rgb: 0x7FDE1B)
let kDefaultViewColorSecondaryGray = UIColor(rgb: 0xCCCCCC)
let kDefaultButtonRedColor = UIColor(rgb: 0xFF0000)
let kDefaultButtonOrangeColor = UIColor(rgb: 0xF3AD52)
let kDefaultButtonGreenColor = UIColor(rgb: 0x219B59)


let kDefaultViewColorTertiary = UIColor(rgb: 0x209624)
let kDefaultViewColorDivider = UIColor(rgb: 0x209624)
let kDefaultViewColorPrimaryLight = UIColor(rgb: 0x209624)
let kDefaultViewColorAccent = UIColor(rgb: 0xFF4081)
//let kDefaultBoaderColor = UIColor(rgb: 0x44891627)

let kDefaultTextColorPrimary = UIColor(rgb: 0x152F72)
let kDefaultTextColorSecondary = UIColor(rgb: 0x555555)
let kDefaultTextColorTertiary = UIColor(rgb: 0xAAAAAA)
let kDefaultTextColorDivider = UIColor(rgb: 0xCFD0CF)
let kDefaultTextColorPrimaryLight = UIColor(rgb: 0x209624)


let kDefaultBackgroundColor = UIColor(rgb: 0xEEEEEE)
let kDefaultButttonPrimary = UIColor(rgb: 0x791D8D)
let kDefaultButtonBoaderColorLight = UIColor(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 0.5)
let kDefaultLineSeprationPrimary = UIColor(rgb: 0xBA9A45)


let kDefaultCellBoaderColorLight = UIColor(rgb:0xE40617)
let kDefaultPaymentBackgroungColor = UIColor(rgb:0xFFDDDC)

let HelveticaNeue = "HelveticaNeue"
let kDefaultFontHelveticaNeueBold = "HelveticaNeue-Bold"
let kDefaultFontHelveticaNeueMedium = "HelveticaNeue-Medium"
let kDefaultFontHelveticaNeueThin = "HelveticaNeue-Thin"

let kDefaultFontAvenirLTStdBook = "AvenirLTStd-Book"
let kDefaultFontAvenirLTStdMedium = "AvenirLTStd-Medium"
let kDefaultFontAvenirLTStdLight = "AvenirLTStd-Light"
let kDefaultFontAvenirLTStdHeavy = "AvenirLTStd-Heavy"


let kDefaultFontLatoBlack = "Lato-Black"
let kDefaultFontLatoBlackItalic = "Lato-BlackItalic"
let kDefaultFontLatoBold = "Lato-Bold"
let kDefaultFontLatoBoldItalic = "Lato-BoldItalic"
let kDefaultFontLatoHairLine = "Lato-Hairline"
let kDefaultFontLatoHairLineItalic = "Lato-HairlineItalic"
let kDefaultFontLatoItalic = "Lato-Italic"
let kDefaultFontLatoLight = "Lato-Light"
let kDefaultFontLatoLightItalic = "Lato-LightItalic"
let kDefaultFontLatoRegular = "Lato-Regular"


let kDefaultFontSizeVerySmall8=8.0
let kDefaultFontSizeVerySmall=10.0
let kDefaultFontSizeSmall11=11.0
let kDefaultFontSizeSmall=12.0
let kDefaultFontSizeMedium13=13.0
let kDefaultFontSizeMedium=14.0
let kDefaultFontSizeLarge15=15.0
let kDefaultFontSizeLarge=16.0
let kDefaultFontSizeExtraLarge=18.0
let kDefaultFontSizeExtraLarge20=20.0
let kDefaultFontSizeExtraLarge22=22.0
let kDefaultFontSizeExtraLarge25=25.0
let kDefaultFontSizeExtraLarge28=28.0
let kDefaultFontSizeExtraLarge30=30.0
let kDefaultFontSizeExtraLarge32=32.0
let kDefaultFontSizeExtraLarge36=36.0
let kDefaultFontSizeExtraLarge40=40.0
let kDefaultFontSizeExtraLarge150=150.0
let base = "https://app.3plfreightsoftware.com/"
let baseUserUrl = "https://app.3plfreightsoftware.com/misc/genericdriverservice.asmx/"
let kPlaceholderImage = "placeholder_section_screen_small"
let userdefualts = UserDefaults.standard
var language = "english"
var lang = 1
var kBaseUserLang = "en"
let MainStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
var dicoLocalisation = NSDictionary()
var LoaderType:Int = 29
let Loadersize = CGSize(width: 30, height: 30)
//var strLoader = localizedStringForKey(key: "loading")
var strLoader = "Loading"
var aBasePER_PAGE_IN_ITEM = 10
var UserIsLogin = false
var ScrenHeight = UIScreen.main.bounds.height
var ScrenWidth = UIScreen.main.bounds.width

let AppuserName = "freightsoftware"
let AppPassword = ""
var DeviceToken = UIDevice.current.identifierForVendor!.uuidString
let appDelegate = UIApplication.shared.delegate as? AppDelegate

var CompanyId = ""
var DriverUserId = Int()
var DriverId = Int()
var DriverUserName = ""

var AcceptRejectFriendRequestMsg = mapping.string(forKey: "Are_you_sure_you_become_friend_key")

let somethingWentWrong = mapping.string(forKey: "Something_went_wrong_key")
let NointernetConnection = mapping.string(forKey: "No_internet_connection_please_try_again_later_key")
let lblLoading = mapping.string(forKey: "Loading_key")

let somethingWentWrongTryAgain = mapping.string(forKey: "Something_went_wrong_try_again_key")


//#MARK:- Google constants
let Scope = "https://www.googleapis.com/auth/contacts.readonly"
let ContactsEndPointURLString = "https://www.google.com/m8/feeds/contacts/default/thin?max-results=10000"
let ClientId = "1010948449312-pq4d5rq9cvp2r93ebo5ga3ke8df73o6r.apps.googleusercontent.com"
// UserDefault Keys
let UserAccessToken = "UserAccessToken"
// Notification Keys
let DataChanged: String = "com.contactshandler.DataChanged"
let TOKEN_NAME:String = "notification_token"
let TOKEN_ID:String = (userdefualts.object(forKey: TOKEN_NAME) == nil) ? "failed" : userdefualts.object(forKey: TOKEN_NAME)! as! String
var isAddvertisementVisible = true



//#MARK:- APIName List

let aBasePostLoginURl = baseUserUrl+"driverLogin"
let aBasePostGetShipmentListURl = baseUserUrl+"shipmentList"
let aBasePostGetSearchShipmentListURl = baseUserUrl+"searchShipmentList"
let aBasePostGetShipmentDetailsURl = baseUserUrl+"shipmentDetails"
let aBasePostGetClockInOutListURl = baseUserUrl+"driverClockInList"
let aBasePostDriverClockInOutURl = baseUserUrl+"driverClockIn"
let aBasePostDriverLogOutURl = baseUserUrl+"driverLogOut"
let aBasePostGetDocTypeListURl = baseUserUrl+"drpDocuments"
let aBasePostCaptureImageURl = baseUserUrl+"uploadShipmentDocument"
let aBasePostCaptureSignatureURl = baseUserUrl+"uploadShipmentSignature"
let aBasePostGetShipmentStatusListURl = baseUserUrl+"drpShipmentStatus"
let aBasePostUpdateShipmentStatusURl = baseUserUrl+"updateShipmentStatus"
let aBasePostUpdateContainerStatusURl = baseUserUrl+"updateContainerChassis"
let aBasePostGetTerminalListURl = baseUserUrl+"drpTerminals"
let aBasePostSaveEmptyContainerURl = baseUserUrl+"updateEmptyContainer"
let aBasePostGetShipmentNotesListURl = baseUserUrl+"drpShipmentNotes"
let aBasePostSaveNotesURl = baseUserUrl+"saveShipmentNotes"
let aBasePostCompleteShipmentURl = baseUserUrl+"completeShipment"
let aBaseGetDimListURl = baseUserUrl+"drpFreightDetails"
let aBaseGetDimURl = baseUserUrl+"dimsList"
let aBaseAddDimURl = baseUserUrl+"addDims"
let aBaseDeleteDimURl = baseUserUrl+"deleteDims"
let aBaseGetWaitingListURl = baseUserUrl+"selectWaitingTime"
let aBasePostUpdateWaitngTimeURl = baseUserUrl+"updateWaitingTime"
let aBaseGetEmptyContainerURl = baseUserUrl+"selecEmptyContainer"
let aBaseGetContainerChassisURl = baseUserUrl+"selectContainerChassis"
let aBasePostUpdateDriverLatLonURl = baseUserUrl+"saveDriverLatLon"
let aBasePostUpdateShipmentLogURl = baseUserUrl+"shipmentLog"
let aBaseSaveDriverGeoFence = baseUserUrl+"saveDriverGeoFence"
