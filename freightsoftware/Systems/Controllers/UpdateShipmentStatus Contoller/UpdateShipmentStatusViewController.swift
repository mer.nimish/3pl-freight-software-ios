//
//  UpdateShipmentStatusViewController.swift
//  freightsoftware
//
//  Created by DK on 20/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import DropDown
import NVActivityIndicatorView

class UpdateShipmentStatusViewController: UIViewController,NVActivityIndicatorViewable {

    //#MARK:- Outlets
    @IBOutlet var lblUpdateStatus: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblStatusValue: UILabel!
    @IBOutlet var btnDone: xUIButton!
    
    //#MARK:- Variable Declare
    
    var ShipmentStatus = -1
    var ShipmentStatusDD = DropDown()
    var theShipmentStatusList:[String] = [String]()
    var ShipmetStatusList = [ShipmentStatusListClass]()
    var controlNumber = Int()
    
    
    //#MARK:- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "UpdateStatus_key"))
        lblUpdateStatus.text! = mapping.string(forKey: "UpdateShipmentStatus_key")
        lblStatus.text! = mapping.string(forKey: "Status_key")
        lblStatusValue.text! = mapping.string(forKey: "Select_key")
        btnDone.setTitle(mapping.string(forKey: "UpdateStatusCapital_key"), for: .normal)
        configDropdown(dropdown: ShipmentStatusDD, sender: lblStatusValue)
        getShipmentList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //#MARK:- Button Action
    
    @IBAction func btnSelectStatusAction(_ sender: UIButton) {
        if(theShipmentStatusList.count > 0)
        {
            showDropDown()
        }
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
        guard controlNumber != 0, DriverUserId != 0, DriverUserName != ""  else {
            self.makeToast(strMessage: mapping.string(forKey: somethingWentWrong))
            return
        }
        guard ShipmentStatus != -1  else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectShipmentStatus_key"))
            return
        }
        
        UpdateStatus()
    }
    
    //#MARK:-  DropDown show/hide method
    
    func showDropDown() {
        ShipmentStatusDD.dataSource = theShipmentStatusList
        ShipmentStatusDD.show()
        ShipmentStatusDD.selectionAction = { (index,item) in
            self.lblStatusValue.text! = item
            self.ShipmentStatus = index
        }
    }
    func hideDropDown() {
        ShipmentStatusDD.hide()
    }
    //#MARK:- Config DropDown
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
     //#MARK:- API Calling
    
    func getShipmentList()
    {
        var para = [String:AnyObject]()
        para["company_ID"] = CompanyId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetShipmentStatusListManager().getShipmentStatusList(param: para) { [weak self] StatusListData in
                
                guard StatusListData == nil else{
                    self!.ShipmetStatusList = StatusListData!
                    StatusListData!.forEach { Status in
                        self!.theShipmentStatusList.append(Status.statusDescription!)
                    }
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    func UpdateStatus()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        para["drpStatusID"] = ShipmetStatusList[ShipmentStatus].id! as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            GetCaptureImageOutManager().postUpdateShipmentStatus(param: para) { [weak self] CaptureSignatureData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard CaptureSignatureData == nil else{
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "UpdateShipmentStatusSuccessfully_key"))
                    self?.navigationController?.popViewController(animated: true)
                    
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
}
