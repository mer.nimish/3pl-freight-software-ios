//
//  ClockInViewController.swift
//  freightsoftware
//
//  Created by DK on 17/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ClockInViewController: UIViewController,NVActivityIndicatorViewable {
    
    //#MARK:- Outlets
    
    @IBOutlet var btnClockInOut: UIButton!
    @IBOutlet var tbl_ClockIn: UITableView!
    
    //#MARK:- Variable Declaration
    let refreshControllerTop  = UIRefreshControl()
    var ClockInList = [ClockInOutClass]()
    
     //#MARK:- View LifCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.btnClockInOut.setTitle(mapping.string(forKey: "ClockIn_key"), for: .normal)
        self.tbl_ClockIn.dataSource = self
        self.tbl_ClockIn.delegate = self
        
        tbl_ClockIn.register(UINib(nibName: "ClockINTableViewCell", bundle: nil), forCellReuseIdentifier: "ClockINTableViewCell")
        tbl_ClockIn.tableFooterView = UIView()
        tbl_ClockIn.estimatedRowHeight = 255
        tbl_ClockIn.rowHeight = UITableViewAutomaticDimension
        refreshControllerTop.tintColor = kDefaultPrimaryColor
        refreshControllerTop.addTarget(self, action: #selector(refreshTheViewData), for: .valueChanged)
        tbl_ClockIn.addSubview(refreshControllerTop)
    }

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationBar(titleText: mapping.string(forKey: "Clock_key"))
        getCheckInOutList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func logout()
    {
        let alertController = UIAlertController(title: mapping.string(forKey: "ThreePL_key"), message: mapping.string(forKey: "Are_you_sure_you_want_to_clockin_out_?_key") , preferredStyle: .alert)
        
        let YesCall = UIAlertAction(title: mapping.string(forKey: "Yes_key"), style: UIAlertActionStyle.default, handler: myHandler)
        
        let dismiss = UIAlertAction(title: mapping.string(forKey: "No_key"), style: UIAlertActionStyle.cancel, handler: NoHandler)
        alertController.addAction(dismiss)
        alertController.addAction(YesCall)
        
        present(alertController, animated: true, completion: nil)
    }
    func myHandler(alert: UIAlertAction){
        postDriverClockInOut()
    }
    func NoHandler(alert: UIAlertAction){
        
    }
    //#MARK:- Button action
    
    @IBAction func btnClockInAction(_ sender: UIButton) {
        logout()
    }
    
    //#MARK:- Member Function
    
    @objc func refreshTheViewData() {
        refreshControllerTop.beginRefreshing()
        getCheckInOutList()
    }
    func startRefreshTheView() {
        self.refreshControllerTop.beginRefreshing()
    }
    func stopRefreshTheView() {
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.refreshControllerTop.endRefreshing()
        })
    }
    //#MARK:- API Calling
    
    func getCheckInOutList(isShouldBegin: Bool = false)
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.ClockInList.removeAll()
            if(!isShouldBegin)
            {
                startRefreshTheView()
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self.tbl_ClockIn.reloadData()
                })
                
            }
            
            GetClockInOutListManager().getClockInOutList(param: para) { [weak self] ClockInOutListData in
                if(!isShouldBegin)
                {
                    self!.stopRefreshTheView()
                }
                
                guard ClockInOutListData == nil else{
                    ClockInOutListData!.forEach { InOut in
                        self!.ClockInList.append(InOut)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                        self!.tbl_ClockIn.reloadData()
                    })
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self!.tbl_ClockIn.reloadData()
                })
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
    func postDriverClockInOut()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            self.btnClockInOut.isUserInteractionEnabled = false
            GetDriverClockInOutManager().getDriverClockInOut(param: para) { [weak self] ClockInOut in
               NetworkActivityManager.sharedInstance.busy = false
               self!.stopAnimating()
                self!.btnClockInOut.isUserInteractionEnabled = true
                guard ClockInOut == nil else{
                    self!.getCheckInOutList(isShouldBegin:true)
                    return
                }
                self!.makeToast(strMessage: somethingWentWrong!)
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
         
    }
    

}
extension ClockInViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if self.ClockInList.count > 0 {
            self.tbl_ClockIn.backgroundView = nil
            numOfSection = 1
            
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbl_ClockIn.bounds.width, height: self.tbl_ClockIn.bounds.height))
            noDataLabel.text = mapping.string(forKey: "NoClockInOut_key")
            noDataLabel.textColor = kDefaultTextColor
            noDataLabel.numberOfLines = 4
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tbl_ClockIn.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.ClockInList.count > 0){
            return self.ClockInList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClockINTableViewCell") as! ClockINTableViewCell
        cell.tag = indexPath.row
        if(self.ClockInList.count > 0){
            cell.setTheData(theModel: self.ClockInList[indexPath.row])
        }
        
        return cell
    }
}


