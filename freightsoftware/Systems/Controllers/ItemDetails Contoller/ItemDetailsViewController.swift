//
//  ItemDetailsViewController.swift
//  freightsoftware
//
//  Created by DK on 18/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import CoreLocation


class ItemDetailsViewController: UIViewController,NVActivityIndicatorViewable {

    //#MARK: - Outlets
    
   
    @IBOutlet var tblDetailsHeights: NSLayoutConstraint!
    @IBOutlet var tblDetails: UITableView!
    @IBOutlet var FromLocationView: xUIView!
    @IBOutlet var lblFromLocation: UILabel!
    @IBOutlet var FromLocationValue: UILabel!
    
    @IBOutlet var ToLocationView: xUIView!
    @IBOutlet var lblToLocation: UILabel!
    @IBOutlet var ToLocationValue: UILabel!
    
    @IBOutlet var MoreVeiwPopup: UIView!
    @IBOutlet var TransprentView: UIView!
    @IBOutlet var btnTransprentClose: UIButton!
    @IBOutlet var DetailsStackView: UIStackView!
    @IBOutlet var viewHome: UIView!
    @IBOutlet var lblHome: UILabel!
    @IBOutlet var viewCaptureImage: UIView!
    @IBOutlet var lblCaptureImage: UILabel!
    @IBOutlet var viewCaptureSignature: UIView!
    @IBOutlet var lblCaptureSignature: UILabel!
    @IBOutlet var viewShipmentStatus: UIView!
    @IBOutlet var lblShipmentStatus: UILabel!
    @IBOutlet var viewUpdateContainer: UIView!
    @IBOutlet var lblUpdateContainer: UILabel!
    @IBOutlet var viewWaitingTime: UIView!
    @IBOutlet var lblWaitingTime: UILabel!
    @IBOutlet var viewFreightDetails: UIView!
    @IBOutlet var lblFreightDetails: UILabel!
    @IBOutlet var viewEmptyContainer: UIView!
    @IBOutlet var lblEmptyContainer: UILabel!
    @IBOutlet var viewNotes: UIView!
    @IBOutlet var lblNotes: UILabel!
    @IBOutlet var viewComplete: UIView!
    @IBOutlet var lblviewComplete: UILabel!
    
    @IBOutlet var MoreViewHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var ConfirmTransperentView: UIView!
    @IBOutlet var viewConfirm: xUIView!
    @IBOutlet var lblConfirm: UILabel!
    @IBOutlet var ConformationMsg: UILabel!    
    @IBOutlet var btnYes: xUIButton!
    @IBOutlet var btnNo: xUIButton!
    
    //#MARK: - Variable Declaration
    var itemId = Int()
    var freightShipment = Bool()
    var DetailsArray = [String]()
    var DetailsValueArray = [String]()
    var MapLoction = String()
    var ToLoctionMapLoction = String()
    var IsMoreMenuOpen = false
    var controlNumber = Int()
    var ContainertControlValue = Int()
    var ContainerValue = String()
    var freightShipmentBool = Bool()
    
    //#MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "Details_key"))
        lblConfirm.text! = mapping.string(forKey: "Confirm_key")
        ConformationMsg.text! = mapping.string(forKey: "ConfirmMsg_key")
        btnYes.setTitle(mapping.string(forKey: "Yes_key"), for: .normal)
        btnNo.setTitle(mapping.string(forKey: "No_key"), for: .normal)
       
        self.tblDetails.dataSource = self
        self.tblDetails.delegate = self
        tblDetails.register(UINib(nibName: "ItemDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemDetailsTableViewCell")
        tblDetails.tableFooterView = UIView()
        tblDetails.estimatedRowHeight = 50
        tblDetails.rowHeight = UITableViewAutomaticDimension
        tblDetailsHeights.constant = 255
        let rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "ic_more_header_white"), style: .done, target: self, action: #selector(self.openMoreViewPopup))
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
         MoreVeiwPopup.isHidden = true
        DetailsStackView.layer.cornerRadius = 4
        self.ConfirmTransperentView.isHidden = true
        self.viewConfirm.isHidden = true
        self.removePopupTitle()
        getItemDetails()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblDetails.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IsMoreMenuOpen = false
        tblDetails.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblDetails.contentSize.height)")
             self.tblDetailsHeights.constant = tblDetails.contentSize.height
        }
    }
    //#MARK: - Button Action
    
    @IBAction func btnMapAction(_ sender: UIButton) {
        if MapLoction != "" && !MapLoction.isEmpty
        {
            let locations = MapLoction.components(separatedBy: ":")
            print(locations)
            let locationLat: String = locations[0]
            let locationlong: String = locations[1]
            //let latitude:CLLocationDegrees = Double(locationLat)!
            //let longitude:CLLocationDegrees =  Double(locationlong)!
            UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?ll=\(locationLat),\(locationlong)")!)
            /*if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(locationLat),\(locationlong)&zoom=14&views=traffic&q=\(locationLat),\(locationlong)")!, options: [:], completionHandler: nil)
                } else {
                    print("http://maps.apple.com/?ll=\(locationLat),\(locationlong)")
                    UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?ll=\(locationLat),\(locationlong)")!)
                }
            } else {
                print("Can't use comgooglemaps://");
            }*/
        }
        
    }
    
    @IBAction func btnToLocationMapAction(_ sender: UIButton) {
        if ToLoctionMapLoction != "" && !ToLoctionMapLoction.isEmpty
        {
            let locations = ToLoctionMapLoction.components(separatedBy: ":")
            print(locations)
            let locationLat: String = locations[0]
            let locationlong: String = locations[1]
            //let latitude:CLLocationDegrees = Double(locationLat)!
            //let longitude:CLLocationDegrees =  Double(locationlong)!
            
            UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?ll=\(locationLat),\(locationlong)")!)
        }
        
       /* if let UrlNavigation = URL.init(string: "comgooglemaps://")
        {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if ToLoctionMapLoction != "" && !ToLoctionMapLoction.isEmpty
                {
                    let locations = MapLoction.components(separatedBy: ":")
                    print(locations)
                    let locationLat: String = locations[0]
                    let locationlong: String = locations[1]
                    
                    if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(locationLat),\(locationlong)&directionsmode=driving") {
                        UIApplication.shared.openURL(urlDestination)
                    }else{
                        UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?ll=\(locationLat),\(locationlong)")!)
                    }
                }
                
               
            }
            else
            {
                if ToLoctionMapLoction != "" && !ToLoctionMapLoction.isEmpty
                {
                    let locations = MapLoction.components(separatedBy: ":")
                    print(locations)
                    let locationLat: String = locations[0]
                    let locationlong: String = locations[1]
                    
                    UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?ll=\(locationLat),\(locationlong)")!)
                }
                
            }
        }
        else
        {
            NSLog("Can't use comgooglemaps://");
            if ToLoctionMapLoction != "" && !ToLoctionMapLoction.isEmpty
            {
                let locations = MapLoction.components(separatedBy: ":")
                print(locations)
                let locationLat: String = locations[0]
                let locationlong: String = locations[1]
                UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?ll=\(locationLat),\(locationlong)")!)
                /*if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(locationLat),\(locationlong)&directionsmode=driving") {
                    UIApplication.shared.openURL(urlDestination)
                }*/
            }
           
        }*/
        
        
    }
    @IBAction func HomeAction(_ sender: UIButton) {
        removeAnimate()
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func CaptureImageAction(_ sender: UIButton) {
        removeAnimate()
        let CaptureImage = self.storyboard?.instantiateViewController(withIdentifier: "CaptureImageViewController") as! CaptureImageViewController
        CaptureImage.controlNumber = self.ContainertControlValue
        self.navigationController?.pushViewController(CaptureImage, animated: false)
    }
    @IBAction func CaptureSignatureAction(_ sender: UIButton) {
        removeAnimate()
        let CaptureImage = self.storyboard?.instantiateViewController(withIdentifier: "CaptureSignatureViewController") as! CaptureSignatureViewController
        CaptureImage.controlNumber = self.ContainertControlValue
        self.navigationController?.pushViewController(CaptureImage, animated: false)
    }
    @IBAction func ShipmentStatusAction(_ sender: UIButton) {
        removeAnimate()
        let UpdateShipmentStatus = self.storyboard?.instantiateViewController(withIdentifier: "UpdateShipmentStatusViewController") as! UpdateShipmentStatusViewController
        UpdateShipmentStatus.controlNumber = self.ContainertControlValue
        self.navigationController?.pushViewController(UpdateShipmentStatus, animated: false)
    }
    @IBAction func UpdateContainerAction(_ sender: UIButton) {
        removeAnimate()
        let UpdateContainer = self.storyboard?.instantiateViewController(withIdentifier: "UpdateContainerStatusViewController") as! UpdateContainerStatusViewController
        UpdateContainer.controlNumber = self.ContainertControlValue
        UpdateContainer.ContainerValue = self.ContainerValue
        self.navigationController?.pushViewController(UpdateContainer, animated: false)
    }
    @IBAction func WatingTimeAction(_ sender: UIButton) {
        removeAnimate()
        if(self.freightShipment)
        {
            let WaitingTimeTrueVC = self.storyboard?.instantiateViewController(withIdentifier: "WaitingTimeTrueViewController") as! WaitingTimeTrueViewController
            WaitingTimeTrueVC.controlNumber = self.ContainertControlValue
            self.navigationController?.pushViewController(WaitingTimeTrueVC, animated: false)
        }else{
            let WaitingTimeVC = self.storyboard?.instantiateViewController(withIdentifier: "WaitingTimeViewController") as! WaitingTimeViewController
            WaitingTimeVC.controlNumber = self.ContainertControlValue
            self.navigationController?.pushViewController(WaitingTimeVC, animated: false)
            
        }
    }
    @IBAction func FreightDetailsAction(_ sender: UIButton) {
        removeAnimate()
        let FreightDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "FreightDetailsViewController") as! FreightDetailsViewController
        FreightDetailsVC.controlNumber = self.ContainertControlValue
        FreightDetailsVC.freightShipment = self.freightShipmentBool
        self.navigationController?.pushViewController(FreightDetailsVC, animated: false)
        
    }
    @IBAction func EmptyContainerAction(_ sender: UIButton) {
        removeAnimate()
        let EmptyContainer = self.storyboard?.instantiateViewController(withIdentifier: "EmptyContainerViewController") as! EmptyContainerViewController
        EmptyContainer.controlNumber = self.ContainertControlValue
        EmptyContainer.ContainerValue = self.ContainerValue
        self.navigationController?.pushViewController(EmptyContainer, animated: false)
        
    }
    @IBAction func NotesAction(_ sender: UIButton) {
        removeAnimate()
        let Notesview = self.storyboard?.instantiateViewController(withIdentifier: "NotesViewController") as! NotesViewController
        Notesview.controlNumber = self.ContainertControlValue
        self.navigationController?.pushViewController(Notesview, animated: false)
    }
    @IBAction func CompleteAction(_ sender: UIButton) {
        removeAnimate()
        showConfirmAnimate()
    }
    
    @objc func openMoreViewPopup(sender: AnyObject) {
        if(IsMoreMenuOpen == false)
        {
            IsMoreMenuOpen = true
            showAnimate()
        }else{
            IsMoreMenuOpen = false
            removeAnimate()
        }
        
    }
    
    @IBAction func btnHideMorePopupAction(_ sender: UIButton) {
        IsMoreMenuOpen = false
        removeAnimate()
    }
    @IBAction func btnCloseConfirmPopup(_ sender: UIButton) {
        removeConfirmAnimate()
    }
    
    @IBAction func btnYesConfirmAction(_ sender: UIButton) {
        removeConfirmAnimate()
        CompleteShipment()
    }
    
    @IBAction func btnNoConfirmAction(_ sender: UIButton) {
        removeConfirmAnimate()
    }
    //#MARK: - Other Member Function
    
    func showAnimate()
    {
        self.MoreVeiwPopup.isHidden = false
        self.TransprentView.alpha = 0.2
        UIView.animate(withDuration: 0.4, animations: {
            self.TransprentView.alpha = 0.5
            if(self.freightShipment)
            {
                self.viewUpdateContainer.isHidden = true
                self.viewEmptyContainer.isHidden = true
                self.viewFreightDetails.isHidden = false
                self.MoreViewHeightConstraint.constant = 350.0
            }else{
                self.viewUpdateContainer.isHidden = false
                self.viewEmptyContainer.isHidden = false
                self.viewFreightDetails.isHidden = true
                self.MoreViewHeightConstraint.constant = 400.0
            }
            
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.TransprentView.alpha = 0.6
            self.setPopupTitle()
        })
        
    }
    
    func removeAnimate()
    {
        
        self.TransprentView.alpha = 0.5
        UIView.animate(withDuration: 0.4, animations: {
            self.TransprentView.alpha = 0.2
            self.MoreViewHeightConstraint.constant = 0.0
            self.removePopupTitle()
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.TransprentView.alpha = 0.0
            self.MoreVeiwPopup.isHidden = true
        })
        
    }
    
    func showConfirmAnimate()
    {
        self.viewConfirm.isHidden = false
        self.ConfirmTransperentView.isHidden = false
        self.ConfirmTransperentView.alpha = 0.2
        UIView.animate(withDuration: 0.4, animations: {
            self.ConfirmTransperentView.alpha = 0.5
        }, completion: { _ in
            self.ConfirmTransperentView.alpha = 0.7
        })
        
    }
    
    func removeConfirmAnimate()
    {
        
        self.ConfirmTransperentView.alpha = 0.6
        UIView.animate(withDuration: 0.4, animations: {
            self.ConfirmTransperentView.alpha = 0.4
        }, completion: { _ in
            self.ConfirmTransperentView.alpha = 0.0
            self.ConfirmTransperentView.isHidden = true
            self.viewConfirm.isHidden = true
        })
        
    }
    func setPopupTitle()
    {
        lblHome.text! = mapping.string(forKey: "Home_key")
        lblCaptureImage.text! = mapping.string(forKey: "CaptureImage_key")
        lblCaptureSignature.text! = mapping.string(forKey: "CaptureSignature_key")
        lblShipmentStatus.text! = mapping.string(forKey: "ShipmentStatus_key")
        lblUpdateContainer.text! = mapping.string(forKey: "UpdateContainer_key")
        lblWaitingTime.text! = mapping.string(forKey: "WaitingTime_key")
        lblFreightDetails.text! = mapping.string(forKey: "FreightDetails_key")
        lblEmptyContainer.text! = mapping.string(forKey: "EmptyContainer_key")
        lblNotes.text! = mapping.string(forKey: "Notes_key")
        lblviewComplete.text! = mapping.string(forKey: "Complete_key")
        
    }
    func removePopupTitle()
    {
        lblHome.text! = mapping.string(forKey: "Empty_key")
        lblCaptureImage.text! = mapping.string(forKey: "Empty_key")
        lblCaptureSignature.text! = mapping.string(forKey: "Empty_key")
        lblShipmentStatus.text! = mapping.string(forKey: "Empty_key")
        lblUpdateContainer.text! = mapping.string(forKey: "Empty_key")
        lblWaitingTime.text! = mapping.string(forKey: "Empty_key")
        lblFreightDetails.text! = mapping.string(forKey: "Empty_key")
        lblEmptyContainer.text! = mapping.string(forKey: "Empty_key")
        lblNotes.text! = mapping.string(forKey: "Empty_key")
        lblviewComplete.text! = mapping.string(forKey: "Empty_key")
    }
    //#MARK: - API calling
    
    func getItemDetails()
    {
        var para = [String:AnyObject]()
        para["id"] = itemId as AnyObject
        para["freightShipment"] = "\(freightShipment)" as AnyObject
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetShipmentDetailsManager().getShipmentDetails(param: para) { [weak self] ItemDetails in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard ItemDetails == nil else{
                    self!.MapLoction = ItemDetails!.fromCoordinates!
                    self!.ToLoctionMapLoction = ItemDetails!.toCoordinates!
                    if let controlValue = ItemDetails!.clientCN
                    {
                        self!.DetailsArray.append(mapping.string(forKey: "Control_key"))
                        self!.DetailsValueArray.append("# \(controlValue)")
                    }
                    if let ContainertValue = ItemDetails!.controlNumber
                    {
                       self!.ContainertControlValue = ContainertValue
                    }
                    if let MoveType = ItemDetails!.moveType, !MoveType.isEmpty
                    {
                        
                        self!.DetailsArray.append(mapping.string(forKey: "MoveType_key"))
                        self!.DetailsValueArray.append(MoveType)
                    }
                    
                    if let Type = ItemDetails!.transactionType, !Type.isEmpty
                    {
                        
                        self!.DetailsArray.append(mapping.string(forKey: "Type_key"))
                        self!.DetailsValueArray.append(Type)
                    }
                    
                    if(self!.freightShipment == false)
                    {
                        if let ContainerNumber = ItemDetails!.containerNumber, !ContainerNumber.isEmpty
                        {
                            self!.DetailsArray.append(mapping.string(forKey: "Container_key"))
                            self!.DetailsValueArray.append("\(ContainerNumber)")
                            self!.ContainerValue = ContainerNumber
                        }
                        
                        if let bookingNumber = ItemDetails!.bookingNumber, !bookingNumber.isEmpty
                        {
                            self!.DetailsArray.append(mapping.string(forKey: "Booking_key"))
                            self!.DetailsValueArray.append(bookingNumber)
                        }
                        
                        if let pin = ItemDetails!.pin, !pin.isEmpty
                        {
                            self!.DetailsArray.append(mapping.string(forKey: "Pin_key"))
                            self!.DetailsValueArray.append("\(pin)")
                        }
                        
                        if let sealNumber = ItemDetails!.sealNumber, !sealNumber.isEmpty
                        {
                            self!.DetailsArray.append(mapping.string(forKey: "Seal_key"))
                            self!.DetailsValueArray.append(sealNumber)
                        }
                        
                        if let railReleaseNumber = ItemDetails!.railReleaseNumber, !railReleaseNumber.isEmpty
                        {
                            
                            self!.DetailsArray.append(mapping.string(forKey: "Release_key"))
                            self!.DetailsValueArray.append(railReleaseNumber)
                        }
                        
                        if let railLocation = ItemDetails!.railLocation, !railLocation.isEmpty
                        {
                            self!.DetailsArray.append(mapping.string(forKey: "Location_key"))
                            self!.DetailsValueArray.append(railLocation)
                        }
                        
                        if let steamShipline = ItemDetails!.steamShipline, !steamShipline.isEmpty
                        {
                            self!.DetailsArray.append(mapping.string(forKey: "ShipmentLine_key"))
                            self!.DetailsValueArray.append(steamShipline)
                        }
                        
                        if let terminalApptDate = ItemDetails!.terminalApptDate, !terminalApptDate.isEmpty
                        {
                            
                            self!.DetailsArray.append(mapping.string(forKey: "TerminalAppt_key"))
                            self!.DetailsValueArray.append(terminalApptDate)
                        }
                        if let terminalAppTimes = ItemDetails!.terminalAppTimes, !terminalAppTimes.isEmpty
                        {
                            self!.DetailsArray.append(mapping.string(forKey: "From_key"))
                            self!.DetailsValueArray.append(terminalAppTimes)
                        }
                        
                        if let terminalApptNumber = ItemDetails!.terminalApptNumber, !terminalApptNumber.isEmpty
                        {
                            
                            self!.DetailsArray.append(mapping.string(forKey: "Appt_key"))
                            self!.DetailsValueArray.append("# \(terminalApptNumber)")
                        }
                    }
                    if let freightDetails = ItemDetails!.freightDetails, !freightDetails.isEmpty
                    {
                        
                        self!.DetailsArray.append(mapping.string(forKey: "Details_key"))
                        self!.DetailsValueArray.append(freightDetails)
                    }
                    self!.tblDetails.reloadData()
                    
                    
                    var ItemDetailsFrom = ""
                    if(self!.freightShipment)
                    {
                        if(ItemDetails!.fromName != nil || ItemDetails!.fromAddress != nil)
                        {
                            ItemDetailsFrom = "\(ItemDetails!.fromName!)\n\(ItemDetails!.fromAddress!)"
                            if(ItemDetails!.fromDate != nil || ItemDetails!.fromTime != nil)
                            {
                                ItemDetailsFrom = ItemDetailsFrom+"\nAppt. \(ItemDetails!.fromDate!) Time \(ItemDetails!.fromTime!)"
                                if(ItemDetails!.fromRef != nil)
                                {
                                    let fromRefArray = ItemDetails!.fromRef!.components(separatedBy: "|")
                                    print(fromRefArray)
                                    fromRefArray.forEach { fromRef in
                                        ItemDetailsFrom = ItemDetailsFrom+"\n\(fromRef)"
                                    }
                                    
                                }
                                
                            }else{
                                if(ItemDetails!.fromRef != nil)
                                {
                                    let fromRefArray = ItemDetails!.fromRef!.components(separatedBy: "|")
                                    fromRefArray.forEach { fromRef in
                                        ItemDetailsFrom = ItemDetailsFrom+"\n\(fromRef)"
                                    }
                                    
                                }
                            }
                        }
                        else{
                            if(ItemDetails!.fromDate != nil || ItemDetails!.fromTime != nil)
                            {
                                ItemDetailsFrom = "Appt. \(ItemDetails!.fromDate!) Time \(ItemDetails!.fromTime!)"
                                if(ItemDetails!.fromRef != nil)
                                {
                                    let fromRefArray = ItemDetails!.fromRef!.components(separatedBy: "|")
                                    
                                    fromRefArray.forEach { fromRef in
                                        ItemDetailsFrom = ItemDetailsFrom+"\n\(fromRef)"
                                    }
                                    
                                }
                                
                            }else{
                                if(ItemDetails!.fromRef != nil)
                                {
                                    let fromRefArray = ItemDetails!.fromRef!.components(separatedBy: "|")
                                    fromRefArray.forEach { fromRef in
                                        ItemDetailsFrom = ItemDetailsFrom+"\n\(fromRef)"
                                    }
                                    
                                }
                            }
                        }
                    }else{
                        if(ItemDetails!.fromName != nil || ItemDetails!.fromAddresss != nil)
                        {
                            ItemDetailsFrom = "\(ItemDetails!.fromName!)\n\(ItemDetails!.fromAddresss!)"
                            if(ItemDetails!.fromDate != nil || ItemDetails!.fromTime != nil)
                            {
                                ItemDetailsFrom = ItemDetailsFrom+"\nAppt. \(ItemDetails!.fromDate!) Time \(ItemDetails!.fromTime!)"
                                if(ItemDetails!.fromRef != nil)
                                {
                                    let fromRefArray = ItemDetails!.fromRef!.components(separatedBy: "|")
                                    fromRefArray.forEach { fromRef in
                                        ItemDetailsFrom = ItemDetailsFrom+"\n\(fromRef)"
                                    }
                                    
                                }
                                
                            }else{
                                if(ItemDetails!.fromRef != nil)
                                {
                                    let fromRefArray = ItemDetails!.fromRef!.components(separatedBy: "|")
                                    fromRefArray.forEach { fromRef in
                                        ItemDetailsFrom = ItemDetailsFrom+"\n\(fromRef)"
                                    }
                                    
                                }
                            }
                        }else{
                            if(ItemDetails!.fromDate != nil || ItemDetails!.fromTime != nil)
                            {
                                ItemDetailsFrom = "Appt. \(ItemDetails!.fromDate!) Time \(ItemDetails!.fromTime!)"
                                if(ItemDetails!.fromRef != nil)
                                {
                                    let fromRefArray = ItemDetails!.fromRef!.components(separatedBy: "|")
                                    fromRefArray.forEach { fromRef in
                                        ItemDetailsFrom = ItemDetailsFrom+"\n\(fromRef)"
                                    }
                                    
                                }
                                
                            }else{
                                if(ItemDetails!.fromRef != nil)
                                {
                                    let fromRefArray = ItemDetails!.fromRef!.components(separatedBy: "|")
                                    fromRefArray.forEach { fromRef in
                                        ItemDetailsFrom = ItemDetailsFrom+"\n\(fromRef)"
                                    }
                                    
                                }
                            }
                        }
                    }
                    self!.FromLocationValue.text! = ItemDetailsFrom
                    
                    
                    var ItemDetailsTo = ""
                    if(ItemDetails!.toName != nil || ItemDetails!.toAddress != nil)
                    {
                        ItemDetailsTo = "\(ItemDetails!.toName!)\n\(ItemDetails!.toAddress!)"
                        if(ItemDetails!.toDate != nil || ItemDetails!.toTime != nil)
                        {
                            ItemDetailsTo = ItemDetailsTo+"\nAppt. \(ItemDetails!.toDate!) Time \(ItemDetails!.toTime!)"
                            if(ItemDetails!.toRef != nil)
                            {
                                let toRefArray = ItemDetails!.toRef!.components(separatedBy: "|")
                                toRefArray.forEach { toRef in
                                    ItemDetailsTo = ItemDetailsTo+"\n\(toRef)"
                                }
                                
                            }
                            
                        }else{
                            if(ItemDetails!.toRef != nil)
                            {
                                let toRefArray = ItemDetails!.toRef!.components(separatedBy: "|")
                                toRefArray.forEach { toRef in
                                    ItemDetailsTo = ItemDetailsTo+"\n\(toRef)"
                                }
                                
                            }
                        }
                    }else{
                        if(ItemDetails!.toDate != nil || ItemDetails!.toTime != nil)
                        {
                            ItemDetailsTo = "\nAppt.\(ItemDetails!.toDate!) Time \(ItemDetails!.toTime!)"
                            if(ItemDetails!.toRef != nil)
                            {
                                let toRefArray = ItemDetails!.toRef!.components(separatedBy: "|")
                                toRefArray.forEach { toRef in
                                    ItemDetailsTo = ItemDetailsTo+"\n\(toRef)"
                                }
                                
                            }
                            
                        }else{
                            if(ItemDetails!.toRef != nil)
                            {
                                let toRefArray = ItemDetails!.toRef!.components(separatedBy: "|")
                                toRefArray.forEach { toRef in
                                    ItemDetailsTo = ItemDetailsTo+"\n\(toRef)"
                                }
                                
                            }
                        }
                    }
                    self!.ToLocationValue.text! = ItemDetailsTo
                    
                    return
                }
                self!.makeToast(strMessage: somethingWentWrong!)
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    func CompleteShipment()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["id"] = itemId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetCaptureImageOutManager().postShipmentComplete(param: para) { [weak self] CaptureImageData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard CaptureImageData == nil else{
                    isShipmentCompleted = true
                    
                    UserDefaults.standard.removeObject(forKey: String(self!.itemId))
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "CompleteShipment_key"))
                    self?.navigationController?.popViewController(animated: true)
                    
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
}
extension ItemDetailsViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.DetailsArray.count > 0){
            return self.DetailsArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemDetailsTableViewCell") as! ItemDetailsTableViewCell
        cell.tag = indexPath.row
        if(self.DetailsArray.count > 0){
            cell.lblTitle.text! = self.DetailsArray[indexPath.row]
            cell.lblDetails.text! = self.DetailsValueArray[indexPath.row]
        }
        return cell
    }
    
}
