//
//  LogInViewController.swift
//  freightsoftware
//
//  Created by DK on 16/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView


class LogInViewController: UIViewController,NVActivityIndicatorViewable,SWRevealViewControllerDelegate{

    //#MARK:- Outlets and Variable declaration
    
    @IBOutlet var lblLogin: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var lblPassword: UILabel!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var imgCheckUnChecked: UIImageView!
    @IBOutlet var lblKeepMeLogin: UILabel!
    @IBOutlet var btnLogin: xUIButton!
    
    //#MARK:- Variable declaration
    var isKeepMeLogin = Bool()
    
    
    //#MARK:- View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        setKeepMeLoginData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setKeepMeLoginData()
    {
        if let isKeepMeLogin:Bool = userdefualts.object(forKey: "isKeepMeLogin") as? Bool {
            if isKeepMeLogin
            {
                print(userdefualts.value(forKey: "isKeepMeEmail") ?? "")
                
                if userdefualts.value(forKey: "isKeepMeEmail") != nil {
                    txtUserName.text! = userdefualts.value(forKey: "isKeepMeEmail") as! String
                }
                print(userdefualts.value(forKey: "isKeepMePassword") ?? "")
                if userdefualts.value(forKey: "isKeepMePassword") != nil {
                    txtPassword.text! = userdefualts.value(forKey: "isKeepMePassword") as! String
                }
                if(isKeepMeLogin)
                {
                    self.isKeepMeLogin = true
                    imgCheckUnChecked.image = #imageLiteral(resourceName: "checkbox_checked")
                }
            }
        }
    }
     //#MARK:- Button Action
    
    @IBAction func KeepMeLoginAction(_ sender: UIButton) {
        if(isKeepMeLogin)
        {
            isKeepMeLogin  = false
            imgCheckUnChecked.image = #imageLiteral(resourceName: "checkbox_unchecked")
        }else{
            isKeepMeLogin  = true
            imgCheckUnChecked.image = #imageLiteral(resourceName: "checkbox_checked")
            
        }
        
    }
    @IBAction func btnLoginAction(_ sender: UIButton) {
        if (txtUserName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage:mapping.string(forKey: "Please_enter_email_address_key"))
            return
        }
        else if !(isValidEmail(emailAddressString: txtUserName.text!))
        {
            makeToast(strMessage: mapping.string(forKey: "Please_enter_valid_email_address_key"))
            return
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: mapping.string(forKey: "Please_enter_password_key"))
            return
        }else if (txtPassword.text?.count)! < 6
        {
            makeToast(strMessage: mapping.string(forKey: "Password_must_be_at_least_6_characters_long_key"))
            
        }
        else{
            Login()
        }
    }
    

    //#MARK:- API Calling
    func Login()
    {
        var para = [String:AnyObject]()
        para["userName"] = txtUserName.text! as AnyObject
        para["userPassword"] = txtPassword.text! as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetLoginManager().getLogin(param: para) { [weak self] LoginData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard LoginData == nil else{                    
                    if LoginData != nil
                    {
                        if(LoginData!.companyID != "" && LoginData!.userID != 0 && LoginData!.driverID != 0 && LoginData!.userName != "")
                        {
                            CompanyId = LoginData!.companyID!
                            DriverUserId = LoginData!.userID!
                            DriverId = LoginData!.driverID!
                            DriverUserName = LoginData!.userName!
                            var LoginResponse = [String:AnyObject]()
                            LoginResponse["CompanyId"] = CompanyId as AnyObject
                            LoginResponse["DriverUserId"] = DriverUserId as AnyObject
                            LoginResponse["DriverId"] = DriverId as AnyObject
                            LoginResponse["DriverUserName"] = DriverUserName as AnyObject
                            
                            userdefualts.removeObject(forKey: "isKeepMeLogin")
                            userdefualts.removeObject(forKey: "LoginData")
                            userdefualts.removeObject(forKey: "isKeepMeEmail")
                            userdefualts.removeObject(forKey: "isKeepMePassword")
                            
                            userdefualts.set(self!.isKeepMeLogin, forKey: "isKeepMeLogin")
                            userdefualts.set(LoginResponse, forKey: "LoginData")
                            
                            userdefualts.set(self!.txtUserName.text!, forKey:"isKeepMeEmail")
                            userdefualts.set(self!.txtPassword.text!, forKey: "isKeepMePassword")
                            userdefualts.synchronize()
                            self!.setupSideMenu()
                            //let HomeViewController = self!.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            //self?.navigationController?.pushViewController(HomeViewController, animated: true)
                        }else{
                            self!.makeToast(strMessage: mapping.string(forKey: "Invalid_username_password_key"))
                        }
                        
                    }
                    
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
    func setupSideMenu()
    {
        
        let frontViewController = MainStoryBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        let rearViewController = MainStoryBoard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        frontNavigationController.navigationBar.isTranslucent = false
        let rearNavigationController = UINavigationController(rootViewController: rearViewController)
        rearNavigationController.navigationBar.isTranslucent = false
        let revealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        //revealController?.panGestureRecognizer().isEnabled = true
        //revealController?.tapGestureRecognizer().isEnabled = true
        revealController?.navigationController?.isNavigationBarHidden = false
        revealController?.rearViewRevealWidth = (frontViewController.view.frame.size.width) - 75
        
        revealController?.delegate = self
        
        let window = UIApplication.shared.delegate?.window as? UIWindow
        
        window?.rootViewController = revealController
        window?.makeKeyAndVisible()
        
    }
}
