//
//  FreightDetailsViewController.swift
//  freightsoftware
//
//  Created by DK on 21/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class FreightDetailsViewController: UIViewController,NVActivityIndicatorViewable {
    
    //#MARK:- Outlets
    
    @IBOutlet var tbl_FreightDetails: UITableView!
    
    
     //#MARK:- Variable Declaration
    var controlNumber = Int()
    var DimTypeSelectedIndex = -1
    var DimQuantity = 0
    var DimWeight = 0
    var DimLenght = 0
    var DimWidth = 0
    var DimHeight = 0
    var dimDim = 194
    var DimTypeList = [FreightDetailsTypeListClass]()
    var theDimTypeList = [String]()
    var SelectedDimType = String()
    var freightShipment = Bool()
    var isHazmat = true
    var DimList = [FreightDetailsDimListClass]()
    var DimDescription = String()
     //#MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "FreightDetails_key"))
        self.tbl_FreightDetails.dataSource = self
        self.tbl_FreightDetails.delegate = self
        tbl_FreightDetails.register(UINib(nibName: "AddDimTableViewCell", bundle: nil), forCellReuseIdentifier: "AddDimTableViewCell")
        tbl_FreightDetails.register(UINib(nibName: "DimListTableViewCell", bundle: nil), forCellReuseIdentifier: "DimListTableViewCell")
        tbl_FreightDetails.tableFooterView = UIView()
        tbl_FreightDetails.estimatedRowHeight = 500
        tbl_FreightDetails.rowHeight = UITableViewAutomaticDimension
        getDimTypeList()
        getDimList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    //#MARK:- API Calling
    func getDimTypeList()
    {
        var para = [String:AnyObject]()
        para["company_ID"] = CompanyId as AnyObject
        para["freightShipment"] = "\(freightShipment)" as AnyObject
        
        print(para)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetDimTypeListManager().getDimTypeList(param: para) { [weak self] DimTypeListData in
                
                guard DimTypeListData == nil else{
                    self!.DimTypeList = DimTypeListData!
                    DimTypeListData!.forEach { DimType in
                        self!.theDimTypeList.append(DimType.drpTypeDesc!)
                    }
                    print(self!.theDimTypeList.count)
                    self!.tbl_FreightDetails.reloadData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getDimList()
    {
        var para = [String:AnyObject]()
        para["controlNumber"] = controlNumber as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.DimList.removeAll()
            self.dimDim = 194
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetDimListManager().getDimList(param: para) { [weak self] DimListData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard DimListData == nil else{
                    self!.DimList = DimListData!
                    self!.dimDim = self!.DimList[0].dimFactor!
                    self!.tbl_FreightDetails.reloadData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    func validateAddDim()
    {
        guard DimQuantity > 0 else {
            makeToast(strMessage: mapping.string(forKey: "PleaseEnterQuantity"))
            return
        }
        guard DimTypeSelectedIndex != -1, SelectedDimType != "" else {
            makeToast(strMessage: mapping.string(forKey: "PleaseSelectType"))
            return
        }
        
        guard DimTypeSelectedIndex != -1, SelectedDimType != "" else {
            makeToast(strMessage: mapping.string(forKey: "PleaseSelectType"))
            return
        }
        guard DimWeight > 0 else {
            makeToast(strMessage: mapping.string(forKey: "PleaseEnterWeight"))
            return
        }
        
        AddDim()

    }
    func AddDim()
    {
        
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        para["id"] = 0 as AnyObject
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = self.controlNumber as AnyObject
        para["dimFactor"] = self.dimDim as AnyObject
        para["pieces"] = DimQuantity as AnyObject
        para["txtDesc"] = DimDescription as AnyObject
        para["l"] = DimLenght as AnyObject
        para["w"] = DimWidth as AnyObject
        para["h"] = DimHeight as AnyObject
        para["drpID"] = DimTypeList[DimTypeSelectedIndex].id! as AnyObject
        para["drpDesc"] = DimTypeList[DimTypeSelectedIndex].drpTypeDesc! as AnyObject
        para["hazMat"] = "\(isHazmat)" as AnyObject
        para["statedWeight"] = DimWeight as AnyObject
        
        print(para)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetAddDimManager().addDim(param: para) { [weak self] DimListData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard DimListData == nil else{
                    
                    self!.DimTypeSelectedIndex = -1
                    self!.DimQuantity = 0
                    self!.DimWeight = 0
                    self!.DimLenght = 0
                    self!.DimWidth = 0
                    self!.DimHeight = 0
                    self!.dimDim = 194
                    self!.SelectedDimType = String()
                    self!.isHazmat = true
                    self!.DimDescription = String()
                    self!.getDimList()
                    self!.tbl_FreightDetails.reloadData()
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "AddDimSuccessfully"))
                    
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func deleteDim(selectedId:Int)
    {
        print(selectedId)
        print(self.DimList[selectedId].id!)
        var para = [String:AnyObject]()
        para["id"] = self.DimList[selectedId].id! as AnyObject
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetAddDimManager().deletDim(param: para) { [weak self] DimListData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard DimListData == nil else{
                    
                    self!.DimTypeSelectedIndex = -1
                    self!.DimQuantity = 0
                    self!.DimWeight = 0
                    self!.DimLenght = 0
                    self!.DimWidth = 0
                    self!.DimHeight = 0
                    self!.dimDim = 194
                    self!.SelectedDimType = String()
                    self!.isHazmat = true
                    self!.DimDescription = String()
                    self!.getDimList()
                    self!.tbl_FreightDetails.reloadData()
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "DeleteDimSuccessfully"))
                    
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    
}
extension FreightDetailsViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.DimList.count > 0){
            return self.DimList.count+1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddDimTableViewCell") as! AddDimTableViewCell
            cell.tag = indexPath.row
            if(self.theDimTypeList.count > 0){
                cell.theDimTypeList = self.theDimTypeList
            }
            cell.setData(NoOfPallets: self.DimList.count,SelectedDimType:self.SelectedDimType, DimQuantity: self.DimQuantity, DimWeight: self.DimWeight, DimLenght: self.DimLenght, DimWidth: self.DimWidth, DimHeight: self.DimHeight, DimValue: self.dimDim,DimDescription:self.DimDescription)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DimListTableViewCell") as! DimListTableViewCell
            cell.tag = indexPath.row
            print(cell.tag)
            if(self.DimList.count > 0)
            {
                cell.setData(theModel: self.DimList[indexPath.row-1])
            }
            return cell
        }
        
    }
    
}

