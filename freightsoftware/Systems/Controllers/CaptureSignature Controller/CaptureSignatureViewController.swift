//
//  CaptureSignatureViewController.swift
//  freightsoftware
//
//  Created by DK on 19/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import EPSignature
import Alamofire
import DropDown
import NVActivityIndicatorView

class CaptureSignatureViewController: UIViewController, EPSignatureDelegate,NVActivityIndicatorViewable {

     //#MARK:- Outlets
    
    @IBOutlet var imgPublicChecked: UIImageView!
    @IBOutlet var lblPublic: UILabel!
    @IBOutlet var lblDocumentType: UILabel!
    @IBOutlet var lblDocumentTypeValue: UILabel!
    @IBOutlet var lblPrintName: UILabel!
    @IBOutlet var txtPrintedName: UITextField!
    @IBOutlet var imgCaptureImage: UIImageView!
    
    @IBOutlet var btnReset: xUIButton!
    @IBOutlet var btnSave: xUIButton!
    
    //#MARK:- Variable Declaration
    
    var userSelectedImageBase64String = String()
    var isPubcilCheck = true
    var DocTypeDD = DropDown()
    var theDocTypeList:[String] = [String]()
    var userSelectedIdex = -1
    var controlNumber = Int()
    var DocTypeArray = [DocTypeClass]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "CaptureSignature_key"))
        lblPublic.text! = mapping.string(forKey: "Public_key")
        lblDocumentType.text! = mapping.string(forKey: "DocumentType_key")
        lblPrintName.text! = mapping.string(forKey: "PrintName_key")
        txtPrintedName.placeholder! = mapping.string(forKey: "EnterHere_key")
        lblDocumentTypeValue.text! = mapping.string(forKey: "Select_key")
        btnReset.setTitle(mapping.string(forKey: "Reset_key"), for: .normal)
        btnSave.setTitle(mapping.string(forKey: "Done_key"), for: .normal)
        imgCaptureImage.layer.cornerRadius = 4
        configDropdown(dropdown: DocTypeDD, sender: lblDocumentTypeValue)
        btnReset.isHidden = true
        getDocTypeList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //#MARK:- Button Action
    
    @IBAction func btnPubcilCheckUnCheck(_ sender: UIButton) {
        if isPubcilCheck
        {
            isPubcilCheck = false
            imgPublicChecked.image = #imageLiteral(resourceName: "checkbox_unchecked")
        }else{
            isPubcilCheck = true
            imgPublicChecked.image = #imageLiteral(resourceName: "checkbox_checked")
        }
    }
    
    @IBAction func btnSelectDocumentType(_ sender: UIButton) {
        if(theDocTypeList.count > 0)
        {
            showDropDown()
        }
    }

    @IBAction func btnSelectImage(_ sender: UIButton) {
        let signatureVC = EPSignatureViewController(signatureDelegate: self, showsDate: false, showsSaveSignatureOption: false)
        // signatureVC.subtitleText = "I agree to the terms and conditions"
        //signatureVC.title = mapping.string(forKey: "CaptureSignature_key")
        signatureVC.title = ""
        signatureVC.tintColor = kDefaultPrimaryColor
        let nav = UINavigationController(rootViewController: signatureVC)
        present(nav, animated: true, completion: nil)
    }
    
    @IBAction func btnResetAction(_ sender: UIButton) {
        imgCaptureImage.image = #imageLiteral(resourceName: "placeholder_signature")
        btnReset.isHidden = true
        userSelectedImageBase64String = String()
        
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        guard controlNumber != 0, DriverUserId != 0, DriverUserName != "", controlNumber != 0, CompanyId != ""  else {
            self.makeToast(strMessage: mapping.string(forKey: somethingWentWrong))
            return
        }
        
        guard userSelectedIdex != -1  else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectDocumentType_key"))
            return
        }
        guard txtPrintedName.text! != ""  else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseEnterPrintedName"))
            return
        }
        guard userSelectedImageBase64String != "" else {
            self.makeToast(strMessage: mapping.string(forKey: "Please_Select_image_key"))
            return
        }
        CaptureSignature()
        
    }
    //#MARK:-  DropDown show/hide method
    
    func showDropDown() {
        DocTypeDD.dataSource = theDocTypeList
        DocTypeDD.show()
        DocTypeDD.selectionAction = { (index,item) in
            self.lblDocumentTypeValue.text! = item
            self.userSelectedIdex = index
            print(self.userSelectedIdex)
        }
    }
    func hideDropDown() {
        DocTypeDD.hide()
    }
    //#MARK:- Config DropDown
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    func epSignature(_: EPSignatureViewController, didCancel error : NSError) {
        print("User canceled")
    }
    
    func epSignature(_: EPSignatureViewController, didSign signatureImage : UIImage, boundingRect: CGRect) {
        print(signatureImage)
        imgCaptureImage.image = signatureImage
        btnReset.isHidden = false
        //let compressData = UIImageJPEGRepresentation(signatureImage, 0.7)
        let jpegCompressionQuality: CGFloat = 0.7 // Set this to whatever suits your purpose
        if let base64String = UIImageJPEGRepresentation(self.imgCaptureImage.image!, jpegCompressionQuality)?.base64EncodedString() {
            userSelectedImageBase64String = base64String
           // imageSize = compressData!.count
            // print("base64String\(base64String)KB")
            // print("Size of Image(\(imageSize)KB)")
        }
    }
    //#MARK:- API Calling
    
    func getDocTypeList()
    {
        var para = [String:AnyObject]()
        para["company_ID"] = CompanyId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetDocTypeManager().getDocType(param: para) { [weak self] DocTypeListData in
                
                guard DocTypeListData == nil else{
                    self!.DocTypeArray = DocTypeListData!
                    DocTypeListData!.forEach { DocType in
                        self!.theDocTypeList.append(DocType.docName!)
                    }
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    func CaptureSignature()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        para["company_ID"] = CompanyId as AnyObject
        para["base64Image"] = userSelectedImageBase64String as AnyObject
        para["contactName"] = txtPrintedName.text! as AnyObject
        para["drpDocumentTypeID"] = DocTypeArray[userSelectedIdex].iD! as AnyObject
        para["drpDocumentType"] = DocTypeArray[userSelectedIdex].docName! as AnyObject
        para["publicStatus"] = "\(isPubcilCheck)" as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            GetCaptureImageOutManager().getCaptureSignature(param: para) { [weak self] CaptureSignatureData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard CaptureSignatureData == nil else{
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "Capture_Signature_successfully"))
                    self?.navigationController?.popViewController(animated: true)
                    
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    

}
