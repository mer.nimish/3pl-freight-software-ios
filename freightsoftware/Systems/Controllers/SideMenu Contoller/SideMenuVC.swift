//
//  SideMenuVC.swift
//  HappyLiving
//
//  Created by Haresh on 11/30/17.
//  Copyright © 2017 Haresh. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import Toaster
import NVActivityIndicatorView

class SideMenuVC: UIViewController,NVActivityIndicatorViewable {

    //MARK: - Outlet
    
    @IBOutlet var UserProfileImg: UIImageView!
    
    @IBOutlet var UserName: UILabel!
    @IBOutlet weak var tblSideList: UITableView!
    
    @IBOutlet weak var vwtblHeader: UIView!
    //MARK: - Variable
    
    var arrayvwBlue = NSMutableArray()
    
    var arrayImg = ["ic_home_sidemenu",
                    "ic_clock_sidemenu",
                    "ic_logout_sidemenu"
                   ]
    
    
    var arraylbl = [mapping.string(forKey: "Home_key"),
                    mapping.string(forKey: "ClockinOut_key"),
                    mapping.string(forKey: "Logout_key")
    ]
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblSideList.register(UINib(nibName: "SideMenuTableCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableCell")
        
        tblSideList.register(UINib(nibName: "SideMenuNotificationCell", bundle: nil), forCellReuseIdentifier: "SideMenuNotificationCell")
        revealViewController().tapGestureRecognizer()
        revealViewController().panGestureRecognizer()
        tblSideList.tableFooterView = UIView()
        tblSideList.tableHeaderView = vwtblHeader
        for _ in 0..<arraylbl.count{
            
            self.arrayvwBlue.add("0")
        }
        
        self.UserProfileImg.layer.cornerRadius = self.UserProfileImg.layer.bounds.width / 2
        self.UserProfileImg.layer.masksToBounds = true
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = kDefaultPrimaryColor
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.tblSideList.reloadData()
        
        self.navigationController?.navigationBar.isHidden = true
        let rewel = self.revealViewController()
        rewel?.frontViewController.view.isUserInteractionEnabled = false
        rewel?.frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        updateHeader()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        let rewel = self.revealViewController()
        rewel?.frontViewController.view.isUserInteractionEnabled = true
    }
    func updateHeader()
    {
        //print(getUserDetail("profile_image"))
         //print(getUserDetail("username"))
        //let url = getUserDetail("profile_image")
       // UserProfileImg.sd_setShowActivityIndicatorView(true)
       // UserProfileImg.sd_setIndicatorStyle(.gray)
       // self.UserProfileImg.sd_setImage(with: URL(string:url), placeholderImage: UIImage(named:"user_placeholder_sidemenu"), options: .lowPriority, completed: nil)
        self.UserName.text = DriverUserName
        
    }
    func logout()
    {
        let alertController = UIAlertController(title: mapping.string(forKey: "ThreePL_key"), message: mapping.string(forKey: "Are_you_sure_you_want_to_logout_?_key") , preferredStyle: .alert)
        
        let YesCall = UIAlertAction(title: mapping.string(forKey: "Yes_key"), style: UIAlertActionStyle.default, handler: myHandler)
        
        let dismiss = UIAlertAction(title: mapping.string(forKey: "No_key"), style: UIAlertActionStyle.cancel, handler: NoHandler)
        alertController.addAction(dismiss)
        alertController.addAction(YesCall)
        
        present(alertController, animated: true, completion: nil)
    }
    func myHandler(alert: UIAlertAction){
        logout(isShowLoader:true)
    }
    func NoHandler(alert: UIAlertAction){
       self.revealViewController().revealToggle(animated: true)
    }
    
    
    
    //#MARK:- API Calling
    
    func logout(isShowLoader:Bool)
    {
        
        guard DriverUserId != 0 else {
            setAlert(msg: "Access Denied")
            return
        }
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetDriverClockInOutManager().getDriverClockInOut(param: para) { [weak self] ClockInOut in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard ClockInOut == nil else{
                    
                    self!.revealViewController().revealToggle(animated: false)
                    self!.RemoveAllLogoutData()
                    let vc : LogInViewController = self!.storyboard!.instantiateViewController(withIdentifier: "LogInVC") as! LogInViewController
                    
                    let rearNavigation = UINavigationController(rootViewController: vc)
                    appDelegate?.window?.rootViewController = rearNavigation
                    return
                }
                self!.makeToast(strMessage: somethingWentWrong!)
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
}

extension SideMenuVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arraylbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell  = tblSideList.dequeueReusableCell(withIdentifier: "SideMenuTableCell") as! SideMenuTableCell
        
        cell.imgSideMenu.image = UIImage(named:arrayImg[indexPath.row])
        cell.lblSideMenu.text = arraylbl[indexPath.row]
        
        let str = arrayvwBlue[indexPath.row] as! String
        
        if str == "1" {
            cell.vwBlue.isHidden = false
            cell.backView.backgroundColor = kDefaultSidemenuSelectedColor
            
        }
        else
        {
            cell.vwBlue.isHidden = true
            cell.backView.backgroundColor = .clear
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<arrayvwBlue.count
        {
            if(i==indexPath.row)
            {
                arrayvwBlue[i] = "1"
                
            }else
            {
                arrayvwBlue[i] = "0"
            }
        }
        
        tblSideList.reloadData()
        
        if indexPath.row == 0
        {
            let HomeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
            let rearNavigation = UINavigationController(rootViewController: HomeVC)
            rearNavigation.isNavigationBarHidden = false
            rearNavigation.navigationBar.isTranslucent = false
            self.revealViewController().rearViewRevealWidth = HomeVC.view.frame.size.width - 75
            self.revealViewController().setFront(rearNavigation, animated: true)
            self.revealViewController().revealToggle(animated: true)
            
        }
        else if indexPath.row == 1
        {
           
            let ClockInVC = self.storyboard?.instantiateViewController(withIdentifier: "ClockInViewController") as! ClockInViewController
            
            let rearNavigation = UINavigationController(rootViewController: ClockInVC)
            rearNavigation.isNavigationBarHidden = false
            rearNavigation.navigationBar.isTranslucent = false
            self.revealViewController().rearViewRevealWidth = ClockInVC.view.frame.size.width - 75
            self.revealViewController().setFront(rearNavigation, animated: true)
            self.revealViewController().revealToggle(animated: true)
            
        }
        else if indexPath.row == 2
        {
           logout()
        }else{
            print("row-->\(indexPath.row)")
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    
}

