//
//  EmptyContainerViewController.swift
//  freightsoftware
//
//  Created by DK on 20/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import DropDown
import NVActivityIndicatorView

class EmptyContainerViewController: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {

    //#MARK:- Outlets
    
    @IBOutlet var lblContainerNumber: UILabel!
    @IBOutlet var txtContainerNumber: UITextField!
    @IBOutlet var lblChasisNumber: UILabel!
    @IBOutlet var txtChasisNumber: UITextField!
    @IBOutlet var lblTerminal: UILabel!
    @IBOutlet var lblTerminalValue: UILabel!    
    @IBOutlet var btnSave: xUIButton!
    
    //#MARK:- Variable Declaration
    var controlNumber = Int()
    var ContainerValue = String()
    var TerminalNumber = -1
    var TerminalDD = DropDown()
    var TerminalList = [TerminalListClass]()
    var theTerminalList:[String] = [String]()
    
    
    //#MARK:- View LifeCycel
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "EmptyContainer_key"))
        lblContainerNumber.text! = mapping.string(forKey: "ContainerNumber_key")
        txtContainerNumber.placeholder! = mapping.string(forKey: "TypeHere_key")
        lblChasisNumber.text! = mapping.string(forKey: "ChasisNumber_key")
        txtChasisNumber.placeholder! = mapping.string(forKey: "TypeHere_key")
        lblTerminal.text! = mapping.string(forKey: "Terminal_key")
        lblTerminalValue.text! = mapping.string(forKey: "SelectHere_key")
        btnSave.setTitle(mapping.string(forKey: "Save_key"), for: .normal)
        print(ContainerValue)
        txtContainerNumber.text! = ContainerValue
        configDropdown(dropdown: TerminalDD, sender: lblTerminalValue)
        getTerminalList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //#MARK:- Button Action
    

    @IBAction func btnSelectStatusAction(_ sender: UIButton) {
        if(TerminalList.count > 0)
        {
            showDropDown()
        }
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        guard controlNumber != 0, DriverUserId != 0, DriverUserName != "", DriverId != 0  else {
            self.makeToast(strMessage: mapping.string(forKey: somethingWentWrong))
            return
        }
        guard ContainerValue != "", !ContainerValue.isEmpty, txtContainerNumber.text != ""  else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseEnterContainerNumber_key"))
            return
        }
        
        guard txtChasisNumber.text != ""  else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseEnterChasisNumber_key"))
            return
        }
        
        guard TerminalNumber != -1  else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectTerminal_key"))
            return
        }
        
        saveEmptyContainer()
    }
    //#MARK:-  DropDown show/hide method
    
    func showDropDown() {
        TerminalDD.dataSource = theTerminalList
        TerminalDD.show()
        TerminalDD.selectionAction = { (index,item) in
            self.lblTerminalValue.text! = item
            self.TerminalNumber = index
        }
    }
    func hideDropDown() {
        TerminalDD.hide()
    }
    //#MARK:- Config DropDown
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == txtContainerNumber)
        {
            ContainerValue = textField.text!
        }
    }
    
    //#MARK:- API Calling
    func getEmptyContainerData()
    {
        var para = [String:AnyObject]()
        para["controlNumber"] = controlNumber as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetEmptyContainerManager().getEmptyContainer(param: para) { [weak self] EmptyContainerData in
                guard EmptyContainerData == nil else{
                    if let containerNumber = EmptyContainerData!.containerNumber
                    {
                        self!.txtContainerNumber.text! = containerNumber
                        self!.ContainerValue = containerNumber
                    }
                    if let chassisNumber = EmptyContainerData!.chassisNumber
                    {
                        self!.txtChasisNumber.text! = chassisNumber
                    }
                    if let terminals = EmptyContainerData!.terminal
                    {
                        var terminalData = ""
                        if let index = self!.TerminalList.index(where: { terminal in
                            if(terminal.iD! == Int(terminals))
                            {
                               return true
                            }
                            return false
                        }){
                            terminalData = self!.TerminalList[index].terminal!
                            self!.TerminalNumber = index
                        }
                       /* self!.TerminalList.forEach { terminal in
                            if(terminal.iD! == Int(terminals))
                            {
                                terminalData = terminal.terminal!
                                self.TerminalNumber =
                            }
                            
                        }*/
                        if(terminalData != "")
                        {
                            self!.lblTerminalValue.text! = terminalData
                            
                        }
                        
                    }
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getTerminalList()
    {
        var para = [String:AnyObject]()
        para["company_ID"] = CompanyId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetTerminalListManager().getTerminalList(param: para) { [weak self] TerminalListData in
                
                guard TerminalListData == nil else{
                    self!.TerminalList = TerminalListData!
                    TerminalListData!.forEach { Status in
                        self!.theTerminalList.append(Status.terminal!)
                    }
                    self!.getEmptyContainerData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    func saveEmptyContainer()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        para["containerNumber"] = ContainerValue as AnyObject
        para["driverID"] = DriverId as AnyObject
        para["chassisNumber"] = txtChasisNumber.text! as AnyObject
        para["terminal"] = TerminalList[TerminalNumber].iD! as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetCaptureImageOutManager().postSaveEmptyContainer(param: para) { [weak self] SaveEmptyContainerData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard SaveEmptyContainerData == nil else{
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "UpdateEmptyContainerSuccessfully_key"))
                    self?.navigationController?.popViewController(animated: true)
                    
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }

}
