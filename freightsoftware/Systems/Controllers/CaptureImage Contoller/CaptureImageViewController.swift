//
//  CaptureImageViewController.swift
//  freightsoftware
//
//  Created by DK on 19/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import DropDown
import NVActivityIndicatorView
import TOCropViewController

class CaptureImageViewController: UIViewController,NVActivityIndicatorViewable {

    //#MARK:- Outlets
    
    @IBOutlet var imgCaptureImage: UIImageView!
    @IBOutlet var captureImgHeightConstraint: NSLayoutConstraint!
    @IBOutlet var captureImgWeightConstraint: NSLayoutConstraint!
    @IBOutlet var imgPublicChecked: UIImageView!
    @IBOutlet var lblPublic: UILabel!
    @IBOutlet var lblDocumentType: UILabel!
    @IBOutlet var lblDocumentTypeValue: UILabel!
    
    @IBOutlet var btnDone: xUIButton!
    //#MARK:- Variable Declaration
    
    let imagePicker = UIImagePickerController()
    var userSelectedImageBase64String = String()
    var isPubcilCheck = true
    var DocTypeDD = DropDown()
    var theDocTypeList:[String] = [String]()
    var userSelectedIdex = -1
    var imageSize = Int()
    var controlNumber = Int()
    var DocTypeArray = [DocTypeClass]()
    
    private var image: UIImage?
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    var imagePickerSourceType = "Camera"
    //#MARK:- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "CaptureImage_key"))
        lblPublic.text! = mapping.string(forKey: "Public_key")
        lblDocumentType.text! = mapping.string(forKey: "DocumentType_key")
        lblDocumentTypeValue.text! = mapping.string(forKey: "Select_key")
        btnDone.setTitle(mapping.string(forKey: "Done_key"), for: .normal)
        captureImgWeightConstraint.constant = ScrenWidth-40
        captureImgHeightConstraint.constant = captureImgWeightConstraint.constant
        imagePicker.delegate = self
        imgCaptureImage.layer.cornerRadius = 4
        configDropdown(dropdown: DocTypeDD, sender: lblDocumentTypeValue)
        getDocTypeList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutImageView()
    }
    public func layoutImageView() {
        /*guard imageView.image != nil else { return }
        
        let padding: CGFloat = 20.0
        
        var viewFrame = self.view.bounds
        viewFrame.size.width -= (padding * 2.0)
        viewFrame.size.height -= ((padding * 2.0))
        
        var imageFrame = CGRect.zero
        imageFrame.size = imgCaptureImage.image!.size;
        
        if imgCaptureImage.image!.size.width > viewFrame.size.width || imgCaptureImage.image!.size.height > viewFrame.size.height {
            let scale = min(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height)
            imageFrame.size.width *= scale
            imageFrame.size.height *= scale
            imageFrame.origin.x = (self.view.bounds.size.width - imageFrame.size.width) * 0.5
            imageFrame.origin.y = (self.view.bounds.size.height - imageFrame.size.height) * 0.5
            imgCaptureImage.frame = imageFrame
        }
        else {
            self.imgCaptureImage.frame = imageFrame;
            self.imgCaptureImage.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        }*/
    }
    //#MARK:- Button Action
    
    @IBAction func btnSelectImage(_ sender: UIButton) {
        let picker = UIImagePickerController()
        picker.delegate = (self as UIImagePickerControllerDelegate & UINavigationControllerDelegate)
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: mapping.string(forKey: "Camera_key"), style: .default, handler: {
            action in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                picker.sourceType = .camera
                self.imagePickerSourceType = "Camera"
                self.present(picker, animated: true, completion: nil)
            }
            else {
                print("Sorry cant take picture")
            }
           
        }))
        alert.addAction(UIAlertAction(title: mapping.string(forKey: "PhotoLibrary_key"), style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            self.imagePickerSourceType = "Photos"
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: mapping.string(forKey: "Cancel_key"), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        /*self.croppingStyle = .default
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)*/
    }
    /*
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        imgCaptureImage.image = image
    }*/
    @IBAction func btnPubcilCheckUnCheck(_ sender: UIButton) {
        if isPubcilCheck
        {
            isPubcilCheck = false
            imgPublicChecked.image = #imageLiteral(resourceName: "checkbox_unchecked")
        }else{
            isPubcilCheck = true
            imgPublicChecked.image = #imageLiteral(resourceName: "checkbox_checked")
        }
    }
    
    @IBAction func btnSelectDocumentType(_ sender: UIButton) {
        if(theDocTypeList.count > 0)
        {
             showDropDown()
        }
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
        
        guard controlNumber != 0, DriverUserId != 0, DriverUserName != "", controlNumber != 0, CompanyId != ""  else {
            self.makeToast(strMessage: mapping.string(forKey: somethingWentWrong))
            return
        }
        guard userSelectedImageBase64String != "", imageSize != 0  else {
            self.makeToast(strMessage: mapping.string(forKey: "Please_Select_image_key"))
            return
        }
        guard userSelectedIdex != -1  else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectDocumentType_key"))
            return
        }
        CaptureImage()
        
    }
    //#MARK:-  DropDown show/hide method
    
    func showDropDown() {
        DocTypeDD.dataSource = theDocTypeList
        DocTypeDD.show()
        DocTypeDD.selectionAction = { (index,item) in
            self.lblDocumentTypeValue.text! = item
            self.userSelectedIdex = index
            print(self.userSelectedIdex)
        }
    }
    func hideDropDown() {
        DocTypeDD.hide()
    }
    //#MARK:- Config DropDown
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    
    //#MARK:- API Calling
    
    func getDocTypeList()
    {
        var para = [String:AnyObject]()
        para["company_ID"] = CompanyId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetDocTypeManager().getDocType(param: para) { [weak self] DocTypeListData in
                
                guard DocTypeListData == nil else{
                    self!.DocTypeArray = DocTypeListData!
                    DocTypeListData!.forEach { DocType in
                        self!.theDocTypeList.append(DocType.docName!)
                    }
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    func CaptureImage()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        para["company_ID"] = CompanyId as AnyObject
        para["base64Image"] = userSelectedImageBase64String as AnyObject
        para["drpDocumentTypeID"] = DocTypeArray[userSelectedIdex].iD! as AnyObject
        para["drpDocumentType"] = DocTypeArray[userSelectedIdex].docName! as AnyObject
        para["fileSize"] = imageSize as AnyObject
        para["publicStatus"] = "\(isPubcilCheck)" as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            GetCaptureImageOutManager().getCaptureImage(param: para) { [weak self] CaptureImageData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard CaptureImageData == nil else{
                    
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "CaptureImageSuccessfully_key"))
                    self?.navigationController?.popViewController(animated: true)
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    /*
    //#MARK:- Cropping Image Methods
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = (info[UIImagePickerControllerOriginalImage] as? UIImage) else { return }
        
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        
        // Uncomment this if you wish to provide extra instructions via a title label
        //cropController.title = "Crop Image"
        
        // -- Uncomment these if you want to test out restoring to a previous crop setting --
        //cropController.angle = 90 // The initial angle in which the image will be rotated
        //cropController.imageCropFrame = CGRect(x: 0, y: 0, width: 2848, height: 4288) //The initial frame that the crop controller will have visible.
        
        // -- Uncomment the following lines of code to test out the aspect ratio features --
        //cropController.aspectRatioPreset = .presetSquare; //Set the initial aspect ratio as a square
        //cropController.aspectRatioLockEnabled = true // The crop box is locked to the aspect ratio and can't be resized away from it
        //cropController.resetAspectRatioEnabled = false // When tapping 'reset', the aspect ratio will NOT be reset back to default
        //cropController.aspectRatioPickerButtonHidden = true
        
        // -- Uncomment this line of code to place the toolbar at the top of the view controller --
        //cropController.toolbarPosition = .top
        
        //cropController.rotateButtonsHidden = true
        //cropController.rotateClockwiseButtonHidden = true
        
        //cropController.doneButtonTitle = "Title"
        //cropController.cancelButtonTitle = "Title"
        
        self.image = image
        
        //If profile picture, push onto the same navigation stack
        if croppingStyle == .circular {
            picker.pushViewController(cropController, animated: true)
        }
        else { //otherwise dismiss, and then present from the main controller
            picker.dismiss(animated: true, completion: {
                self.present(cropController, animated: true, completion: nil)
                //self.navigationController!.pushViewController(cropController, animated: true)
            })
        }
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
 
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        //imgCaptureImage.image = image
        let compressData = UIImageJPEGRepresentation(image, 0.5) //max value is 1.0 and minimum is 0.0
        self.imgCaptureImage.image = UIImage(data: compressData!);
        let jpegCompressionQuality: CGFloat = 0.5 // Set this to whatever suits your purpose
        if let base64String = UIImageJPEGRepresentation(self.imgCaptureImage.image!, jpegCompressionQuality)?.base64EncodedString() {
            userSelectedImageBase64String = base64String
            imageSize = compressData!.count
            // print("base64String\(base64String)KB")
             print("Size of Image(\(imageSize)KB)")
        }
        layoutImageView()
        
        if cropViewController.croppingStyle != .circular {
            
            cropViewController.dismissAnimatedFrom(self, withCroppedImage: image,
                                                   toView: imgCaptureImage,
                                                   toFrame: CGRect.zero,
                                                   setup: { self.layoutImageView() },
                                                   completion: { })
        }
        else {
            cropViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    */
    
    
}
/*
extension CaptureImageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let compressData = UIImageJPEGRepresentation(image, 0.5) //max value is 1.0 and minimum is 0.0
            self.imgCaptureImage.image = UIImage(data: compressData!);
            let jpegCompressionQuality: CGFloat = 0.5 // Set this to whatever suits your purpose
            if let base64String = UIImageJPEGRepresentation(self.imgCaptureImage.image!, jpegCompressionQuality)?.base64EncodedString() {
                userSelectedImageBase64String = base64String
                imageSize = compressData!.count
               // print("base64String\(base64String)KB")
               // print("Size of Image(\(imageSize)KB)")
            }
            
        } else{
            print("Something went wrong")
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}*/
/////////////
extension CaptureImageViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate
{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let selectedImage = info[UIImagePickerControllerOriginalImage]
        let cropVC = TOCropViewController(image: selectedImage as! UIImage)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
       // cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        cropVC.customAspectRatio = CGSize(width: 815, height: 1055)
        //
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.orange, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.orange, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        print("imagePickerSourceType-->\(imagePickerSourceType)")
        if (imagePickerSourceType == "Camera") {
            //image taken with camera
            picker.dismiss(animated: true, completion: {
                self.present(cropVC, animated: true, completion: nil)
            })
        }
        else{
            //image taken from camera roll
            cropVC.cancelButtonTitle = "Back"
            picker.pushViewController(cropVC, animated: true)
        }

        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        let comImage = image.resizeImage(targetSize: CGSize(width: 815, height: 1055))
        let compressData = UIImageJPEGRepresentation(comImage, 0.5) //max value is 1.0 and minimum is 0.0
        self.imgCaptureImage.image = UIImage(data: compressData!);
        let imagewidth = self.imgCaptureImage.image?.size.width
        let imageheight = self.imgCaptureImage.image?.size.height
        print(imagewidth,imageheight)
        let jpegCompressionQuality: CGFloat = 0.5 // Set this to whatever suits your purpose
        if let base64String = UIImageJPEGRepresentation(self.imgCaptureImage.image!, jpegCompressionQuality)?.base64EncodedString() {
            userSelectedImageBase64String = base64String
            imageSize = compressData!.count
            // print("base64String\(base64String)KB")
             print("Size of Image(\(imageSize)KB)")
            /*let dataDecoded : Data = Data(base64Encoded: base64String, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            testImg.image = decodedimage*/
        }
        cropViewController.dismiss(animated: false, completion: nil)
        //imagePicker.dismiss(animated: true, completion: nil)
        dismiss(animated: true, completion: nil)
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
