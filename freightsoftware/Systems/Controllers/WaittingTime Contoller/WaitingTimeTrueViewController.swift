//
//  WaitingTimeTrueViewController.swift
//  freightsoftware
//
//  Created by DK on 23/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class WaitingTimeTrueViewController: UIViewController,UIScrollViewDelegate,NVActivityIndicatorViewable {
    //#MARK:- Outlets
    
    @IBOutlet var WaitingTimeScrollView: UIScrollView!
    @IBOutlet var lblFrom: UILabel!
    @IBOutlet var lblFromStartTime: labelDesinable!
    @IBOutlet var lblFromDateTime: UILabel!
    @IBOutlet var btnFromClear: UIButton!
    @IBOutlet var lblFromDateTimeValue: UILabel!
    @IBOutlet var txtFromStartTime: UITextField!
    
    @IBOutlet var lblEndStartTime: labelDesinable!
    @IBOutlet var lblEndDateTime: UILabel!
    @IBOutlet var btnEndClear: UIButton!
    @IBOutlet var lblEndDateTimeValue: UILabel!
    @IBOutlet var txtFromEndTime: UITextField!
    
    
    
    @IBOutlet var lblDelivery: UILabel!
    @IBOutlet var lblDeliveryStartTime: labelDesinable!
    @IBOutlet var lblDeliveryDateTime: UILabel!
    @IBOutlet var btnDeliveryClear: UIButton!
    @IBOutlet var lblDeliveryDateTimeValue: UILabel!
    @IBOutlet var txtDeliveryStartTime: UITextField!
    
    @IBOutlet var lblDeliveryEndStartTime: labelDesinable!
    @IBOutlet var lblDeliveryEndDateTime: UILabel!
    @IBOutlet var btnDeliveryEndClear: UIButton!
    @IBOutlet var lblDeliveryEndDateTimeValue: UILabel!
    @IBOutlet var txtDeliveryEndTime: UITextField!
    
    @IBOutlet var btnSave: xUIButton!
    
    
     //#MARK:- Variable Declared
    
    let datePicker = UIDatePicker()
    var isDate = "-1"
    var tempDate = ""
    var selectPlaceholder = mapping.string(forKey: "Select_key")
    var controlNumber = Int()
    
     //#MARK:- View lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "WaitingTime_key"))
        setMappingValue()
        getWaitingList()
        self.WaitingTimeScrollView.delegate = self
        [txtFromStartTime,txtFromEndTime,txtDeliveryStartTime,txtDeliveryEndTime].forEach { (textfield) in
            textfield?.delegate = self
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setMappingValue()
    {
        lblFrom.text = mapping.string(forKey: "From_key")
        lblFromStartTime.text = mapping.string(forKey: "StartTime_key")
        lblFromDateTime.text = mapping.string(forKey: "DateTime_key")
        btnFromClear.setTitle(mapping.string(forKey: "Clear_key"), for: .normal)
        btnFromClear.isHidden = true
        lblFromDateTimeValue.text = selectPlaceholder
        
        
        lblEndStartTime.text = mapping.string(forKey: "EndTime_key")
        lblEndDateTime.text = mapping.string(forKey: "DateTime_key")
        btnEndClear.setTitle(mapping.string(forKey: "Clear_key"), for: .normal)
        btnEndClear.isHidden = true
        lblEndDateTimeValue.text = selectPlaceholder
        
        lblDelivery.text = mapping.string(forKey: "Delivery_key")
        lblDeliveryStartTime.text = mapping.string(forKey: "StartTime_key")
        lblDeliveryDateTime.text = mapping.string(forKey: "DateTime_key")
        btnDeliveryClear.setTitle(mapping.string(forKey: "Clear_key"), for: .normal)
        btnDeliveryClear.isHidden = true
        lblDeliveryDateTimeValue.text = selectPlaceholder
        
        lblDeliveryEndStartTime.text = mapping.string(forKey: "EndTime_key")
        lblDeliveryEndDateTime.text = mapping.string(forKey: "DateTime_key")
        btnDeliveryEndClear.setTitle(mapping.string(forKey: "Clear_key"), for: .normal)
        btnDeliveryEndClear.isHidden = true
        lblDeliveryEndDateTimeValue.text = selectPlaceholder
        
        
    }
     //#MARK:- Button Actions
    
    @IBAction func btnFromStartClearAction(_ sender: UIButton) {
        lblFromDateTimeValue.text = selectPlaceholder
        self.tempDate = ""
        isDate = "-1"
        btnFromClear.isHidden = true
    }
    
    @IBAction func btnFromEndClearAction(_ sender: UIButton) {
        lblEndDateTimeValue.text = selectPlaceholder
        self.tempDate = ""
        isDate = "-1"
        btnEndClear.isHidden = true
    }
    @IBAction func btnDeliveryStartClearAction(_ sender: UIButton) {
        lblDeliveryDateTimeValue.text = selectPlaceholder
        self.tempDate = ""
        isDate = "-1"
        btnDeliveryClear.isHidden = true
    }
    
    @IBAction func btnDeliveryEndClearAction(_ sender: UIButton) {
        lblDeliveryEndDateTimeValue.text = selectPlaceholder
        self.tempDate = ""
        isDate = "-1"
        btnDeliveryEndClear.isHidden = true

    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        if(self.lblFromDateTimeValue.text == selectPlaceholder || self.lblFromDateTimeValue.text == "")
        {
            self.lblFromDateTimeValue.text = ""
        }
        if(self.lblEndDateTimeValue.text == selectPlaceholder || self.lblEndDateTimeValue.text == "")
        {
            self.lblEndDateTimeValue.text = ""
        }
        if(self.lblDeliveryDateTimeValue.text == selectPlaceholder || self.lblDeliveryDateTimeValue.text == "")
        {
            self.lblDeliveryDateTimeValue.text = ""
        }
        
        if(self.lblDeliveryEndDateTimeValue.text == selectPlaceholder || self.lblDeliveryEndDateTimeValue.text == "")
        {
            self.lblDeliveryEndDateTimeValue.text = ""
        }
        UpdateWaitingTime()
    }
    
    //#MARK:- Other Mothods
    
    func setTimer()
    {
            let obj = MainStoryBoard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.isSetMinimumDate = true
            obj.isComeFromPool = true
            obj.pickerDelegate = self
            obj.controlType = 0
            obj.isTimerOpen = true
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
    }
     //#MARK:- API Calling
    func getWaitingList()
    {
        var para = [String:AnyObject]()
        para["controlNumber"] = controlNumber as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetWaitingListManager().getWaitingList(param: para) { [weak self] WaitingListData in
                guard WaitingListData == nil else{
                    if let pickupFrom = WaitingListData!.pickupFrom
                    {
                        if let number = Int(pickupFrom.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                            let datee = Date(milliseconds: number)
                            self!.lblFromDateTimeValue.text! = self!.DateToString(Formatter:"M/dd/yyyy hh:mm a",date:datee)
                            
                        }
                    }
                    if let pickupTo = WaitingListData!.pickupTo
                    {
                        if let number = Int(pickupTo.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                            let datee = Date(milliseconds: number)
                            self!.lblEndDateTimeValue.text! = self!.DateToString(Formatter:"M/dd/yyyy hh:mm a",date:datee)
                            
                        }
                    }
                    if let deliveryFrom = WaitingListData!.deliveryFrom
                    {
                        if let number = Int(deliveryFrom.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                            let datee = Date(milliseconds: number)
                            self!.lblDeliveryDateTimeValue.text! = self!.DateToString(Formatter:"M/dd/yyyy hh:mm a",date:datee)
                            
                        }
                    }
                    if let deliveryTo = WaitingListData!.deliveryTo
                    {
                        if let number = Int(deliveryTo.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                            let datee = Date(milliseconds: number)
                            self!.lblDeliveryEndDateTimeValue.text! = self!.DateToString(Formatter:"M/dd/yyyy hh:mm a",date:datee)
                            
                        }
                    }
                    return
                }
            }
        }
        else
        {
            makeToastWithBack(strMessage: NointernetConnection!)
        }
    }
    func UpdateWaitingTime()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        para["company_ID"] = CompanyId as AnyObject
        para["pickupFrom"] = self.lblFromDateTimeValue.text! as AnyObject
        para["pickupTo"] = self.lblEndDateTimeValue.text! as AnyObject
        para["deliveryFrom"] = self.lblDeliveryDateTimeValue.text! as AnyObject
        para["deliveryTo"] = self.lblDeliveryEndDateTimeValue.text! as AnyObject
        para["returnFrom"] = "" as AnyObject
        para["returnTo"] = "" as AnyObject
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            GetCaptureImageOutManager().postUpdateWaitingTime(param: para) { [weak self] CaptureImageData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                self!.lblFromDateTimeValue.text = self!.selectPlaceholder
                self!.lblEndDateTimeValue.text = self!.selectPlaceholder
                self!.lblDeliveryDateTimeValue.text = self!.selectPlaceholder
                self!.lblDeliveryEndDateTimeValue.text = self!.selectPlaceholder
                guard CaptureImageData == nil else{
                   
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "WaitingTimeSuccessfully_key"))
                     self?.navigationController?.popViewController(animated: true)
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }

}
//MARKL: - TextField delgate

extension WaitingTimeTrueViewController : UITextFieldDelegate
{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
         if textField == txtFromStartTime
        {
            isDate = "0"
            
         }else if textField == txtFromEndTime
         {
            isDate = "1"
            
         }
         else if textField == txtDeliveryStartTime
         {
            isDate = "2"
            
         }
         else if textField == txtDeliveryEndTime
         {
            isDate = "3"
            
         }
        else
        {
            isDate = "-1"
        }
        let obj = MainStoryBoard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
        obj.pickerDelegate = self
        obj.isComeFromPool = true
        obj.controlType = 1
        obj.isSetMaximumDate = true
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
        return false
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
   
    
}

extension WaitingTimeTrueViewController : DateTimePickerDelegate
{
    func setDateandTime(dateValue: Date,type : Int) {
        
        if type == 1
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "M/dd/yyyy"
            //lblFromDateTimeValue.text = dateFormatter.string(from: dateValue)
            self.tempDate = dateFormatter.string(from: dateValue)
            setTimer()
        }
        else if type == 0
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            if isDate == "0"
            {
                lblFromDateTimeValue.text = "\(self.tempDate) "+dateFormatter.string(from: dateValue)
                btnFromClear.isHidden = false
            }
            else if isDate == "1"
            {
                lblEndDateTimeValue.text = "\(self.tempDate) "+dateFormatter.string(from: dateValue)
                btnEndClear.isHidden = false
            }
            else if isDate == "2"
            {
                lblDeliveryDateTimeValue.text = "\(self.tempDate) "+dateFormatter.string(from: dateValue)
                btnDeliveryClear.isHidden = false
            }
            else if isDate == "3"
            {
                lblDeliveryEndDateTimeValue.text = "\(self.tempDate) "+dateFormatter.string(from: dateValue)
                btnDeliveryEndClear.isHidden = false
            }
            else
            {
                //txtSecondarybirthDate.text = dateFormatter.string(from: dateValue)
                
                
                

            }
            isDate = "-1"
            self.tempDate = ""
        }
        
    }
}

