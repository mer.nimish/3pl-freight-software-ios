//
//  NotesViewController.swift
//  freightsoftware
//
//  Created by DK on 20/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import NVActivityIndicatorView

class NotesViewController: UIViewController,NVActivityIndicatorViewable,UITextViewDelegate{

    //#MARK: - Outlets
    
    @IBOutlet var PublicStatEnableDisable: UIImageView!
    @IBOutlet var lblPublic: UILabel!
    @IBOutlet var lblNotesType: UILabel!
    @IBOutlet var lblNotesValue: UILabel!
    @IBOutlet var lblNotes: UILabel!
    @IBOutlet var txtNotes: TextViewDesinable!
    @IBOutlet var btnSave: xUIButton!
    
    
    //#MARK: - Variable Declare
    
    var isPubcilCheck = true
    var NotesIndex = -1
    var NotesListDD = DropDown()
    var theNotesList:[String] = [String]()
    var NotesListList = [ShimpentNotesListClass]()
    var controlNumber = Int()
    var placeholder = mapping.string(forKey: "Enter_here_key")
    var NotesTypeplaceholder = mapping.string(forKey: "Select_key")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "Notes_key"))
        lblPublic.text! = mapping.string(forKey: "Public_key")
        lblNotesType.text! = mapping.string(forKey: "NotesTypeCapital_key")
        lblNotesValue.text! = mapping.string(forKey: "Select_key")
        lblNotes.text! = mapping.string(forKey: "NotesCapital_key")
        txtNotes.text = placeholder
        txtNotes.textColor = UIColor.lightGray
        btnSave.setTitle(mapping.string(forKey: "Save_key"), for: .normal)
        configDropdown(dropdown: NotesListDD, sender: lblNotesValue)
        getNotesList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //#MARK: - Button action.
    @IBAction func btnPubcilCheckUnCheck(_ sender: UIButton) {
        if isPubcilCheck
        {
            isPubcilCheck = false
            PublicStatEnableDisable.image = #imageLiteral(resourceName: "checkbox_unchecked")
        }else{
            isPubcilCheck = true
            PublicStatEnableDisable.image = #imageLiteral(resourceName: "checkbox_checked")
        }
    }
    
    @IBAction func btnSelectDocumentType(_ sender: UIButton) {
        if(theNotesList.count > 0)
        {
            showDropDown()
        }
    }

    @IBAction func btnSaveAction(_ sender: UIButton) {
        
        guard controlNumber != 0, DriverUserId != 0, DriverUserName != "" else {
            makeToast(strMessage: mapping.string(forKey: somethingWentWrong))
            return
        }
        
        guard self.lblNotesValue.text != "", !self.lblNotesValue.text!.isEmpty, self.lblNotesValue.text != NotesTypeplaceholder else {
            makeToast(strMessage: mapping.string(forKey: "PleaseSelectNotesTypes_key"))
            return
        }
        guard NotesIndex != -1 else {
            makeToast(strMessage: mapping.string(forKey: "PleaseSelectNotesTypes_key"))
            return
        }
        
        guard self.txtNotes.text != "", !self.txtNotes.text!.isEmpty, self.txtNotes.text != placeholder else {
            makeToast(strMessage: mapping.string(forKey: "PleaseEnterNotes_key"))
            return
        }
        saveNotes()
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = kDefaultSeparationLineColorPrimary
        }
    }
    //#MARK:-  DropDown show/hide method
    
    func showDropDown() {
        NotesListDD.dataSource = theNotesList
        NotesListDD.show()
        NotesListDD.selectionAction = { (index,item) in
            self.lblNotesValue.text! = item
            self.NotesIndex = index
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        

        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = UIColor.lightGray
        }
    }
    func hideDropDown() {
        NotesListDD.hide()
    }
    //#MARK:- Config DropDown
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    
    //#MARK:- API Calling
    
    func getNotesList()
    {
        var para = [String:AnyObject]()
        para["company_ID"] = CompanyId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetNotesListManager().getNotesList(param: para) { [weak self] NotesListListData in
                guard NotesListListData == nil else{
                    self!.NotesListList = NotesListListData!
                    NotesListListData!.forEach { Notes in
                        self!.theNotesList.append(Notes.noteType!)
                    }
                    return
                }
            }
        }
        else
        {
            makeToastWithBack(strMessage: NointernetConnection!)
        }
    }
    func saveNotes()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        para["userNotes"] = txtNotes.text! as AnyObject
        para["contactName"] = "" as AnyObject
        para["drpDocumentTypeID"] = NotesListList[NotesIndex].id! as AnyObject
        para["publicStatus"] = "\(isPubcilCheck)" as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            GetCaptureImageOutManager().postSaveNotes(param: para) { [weak self] SaveNotesData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard SaveNotesData == nil else{
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "SaveNotesSuccessfully_key"))
                    self?.navigationController?.popViewController(animated: true)
                    
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
}
