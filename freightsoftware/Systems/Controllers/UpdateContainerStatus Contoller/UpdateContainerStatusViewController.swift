//
//  UpdateContainerStatusViewController.swift
//  freightsoftware
//
//  Created by DK on 20/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class UpdateContainerStatusViewController: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {
    
    //#MARK:- Outlets
    
    @IBOutlet var lblUpdateContainer: UILabel!
    @IBOutlet var lblContainer: UILabel!
    @IBOutlet var txtContainerValue: UITextField!
    @IBOutlet var lblChasis: UILabel!
    @IBOutlet var txtChasisValue: UITextField!
    @IBOutlet var lblSeal: UILabel!
    @IBOutlet var txtSealValue: UITextField!
    @IBOutlet var btnDone: xUIButton!
    
    //#MARK:- Variable Declare
    var controlNumber = Int()
    var ContainerValue = String()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "UpdateContainer_key"))
        lblUpdateContainer.text! = mapping.string(forKey: "UpdateContainerCapital_key")
        lblContainer.text! = mapping.string(forKey: "ContainerCapital_key")
        txtContainerValue.placeholder! = mapping.string(forKey: "TypeHere_key")
        lblChasis.text! = mapping.string(forKey: "Chasis_key")
        txtChasisValue.placeholder! = mapping.string(forKey: "TypeHere_key")
        lblSeal.text! = mapping.string(forKey: "SealCapital_key")
        txtSealValue.placeholder! = mapping.string(forKey: "TypeHere_key")
        btnDone.setTitle(mapping.string(forKey: "UpdateContainerCapital_key"), for: .normal)
        txtContainerValue.text! = ContainerValue
        getUpdateContainerData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //#MARK: - Button Action
    
    @IBAction func btnUpdateStatusAction(_ sender: UIButton) {
         guard controlNumber != 0, DriverUserId != 0, DriverUserName != ""  else {
            self.makeToast(strMessage: mapping.string(forKey: somethingWentWrong))
            return
        }
        guard ContainerValue != "", !ContainerValue.isEmpty, txtContainerValue.text != "" else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseEnterContainerNumber_key"))
            return
        }
        guard  txtChasisValue.text != "" else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseEnterChasisNumber_key"))
            return
        }
        /*guard  txtSealValue.text != "" else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseEnterSealNumber_key"))
            return
        }*/
        UpdateStatus()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == txtContainerValue)
        {
            ContainerValue = textField.text!
        }
    }
    
    //#MARK:- API Calling
    func getUpdateContainerData()
    {
        var para = [String:AnyObject]()
        para["controlNumber"] = controlNumber as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetContainerChassisManager().getContainerChassis(param: para) { [weak self] UpdateContainerData in
                guard UpdateContainerData == nil else{
                    if let container = UpdateContainerData!.container
                    {
                        self!.txtContainerValue.text! = container
                        self!.ContainerValue = container
                    }
                    if let chassisNumber = UpdateContainerData!.chassisNumber
                    {
                        self!.txtChasisValue.text! = chassisNumber
                    }
                    if let sealNumber = UpdateContainerData!.sealNumber
                    {
                        self!.txtSealValue.text! = sealNumber
                        
                    }
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    func UpdateStatus()
    {
        var para = [String:AnyObject]()
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["controlNumber"] = controlNumber as AnyObject
        para["containerNumber"] = ContainerValue as AnyObject
        para["chassisNumber"] = txtChasisValue.text! as AnyObject
        para["sealNumber"] = txtSealValue.text! as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            GetCaptureImageOutManager().postUpdateContainerStatus(param: para) { [weak self] UpdateContainerStatusData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard UpdateContainerStatusData == nil else{
                    self!.makeToastWithBack(strMessage: mapping.string(forKey: "UpdateContainerStatusSuccessfully_key"))
                    self?.navigationController?.popViewController(animated: true)
                    
                    return
                }
                self!.makeToast(strMessage: mapping.string(forKey: somethingWentWrong))
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
}
