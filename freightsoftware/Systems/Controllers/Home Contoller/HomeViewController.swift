//
//  HomeViewController.swift
//  freightsoftware
//
//  Created by DK on 16/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
var isShipmentCompleted = false

class HomeViewController: UIViewController {
    
    //#MARK:- Outlets
    
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var tbl_Shipment: UITableView!
    
     //#MARK:- Variable Declaration
    let refreshControllerTop  = UIRefreshControl()
    var ShipmentList = [ShipmentListClass]()
 
    

     //#MARK:- View LifCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.tbl_Shipment.dataSource = self
        self.tbl_Shipment.delegate = self
       
        tbl_Shipment.register(UINib(nibName: "HomeVCTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeVCTableViewCell")
        
        tbl_Shipment.tableFooterView = UIView()
        tbl_Shipment.estimatedRowHeight = 255
        tbl_Shipment.rowHeight = UITableViewAutomaticDimension
        refreshControllerTop.tintColor = kDefaultPrimaryColor
        refreshControllerTop.addTarget(self, action: #selector(refreshTheViewData), for: .valueChanged)
        tbl_Shipment.addSubview(refreshControllerTop)
        getShipmentList()
       
    }

    override func viewWillAppear(_ animated: Bool) {
         setupNavigationBar(titleText: mapping.string(forKey: "freightsoftware_key"))
        if(isShipmentCompleted)
        {
            isShipmentCompleted = false
            self.stopMonitoring()
            getShipmentList()
        }
        var counter = 0
        if UserDefaults.standard.object(forKey: "LocationCounter") != nil {
            counter = UserDefaults.standard.object(forKey: "LocationCounter") as! Int
        }
        print("counter-->",counter)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //#MARK:- Member Function
    
    @objc func refreshTheViewData() {
        refreshControllerTop.beginRefreshing()
        getShipmentList()
    }
    func startRefreshTheView() {
        self.refreshControllerTop.beginRefreshing()
        self.stopMonitoring()
    }
    func stopRefreshTheView() {
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.refreshControllerTop.endRefreshing()
        })
    }
    func setMonitoredRegion(lati:Double,longi:Double,id:Int,FromTo:String,ShipmentId:Int) {
        let startLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lati, longitude: longi)
        let monitoredRegion = CLCircularRegion(center: startLocation, radius: 100, identifier: "\(ShipmentId)-\(FromTo)\(id)")
        //Before => "ContainerId-\(FromTo)\(id)"
       let str =  monitoredRegion.identifier
        let regions = str.components(separatedBy: "-")
        let ShipmentId = regions[0]
        print(ShipmentId)
        let Logs = regions[1].components(separatedBy: "_")
        print(Logs)
        monitoredRegion.notifyOnEntry = true
        monitoredRegion.notifyOnExit = true
        appDelegate?.locationManager.startMonitoring(for: monitoredRegion)
    }
    func stopMonitoring() {
        for region in (appDelegate?.locationManager.monitoredRegions)! {
            guard let circularRegion = region as? CLCircularRegion else { continue }
            appDelegate?.locationManager.stopMonitoring(for: circularRegion)
        }
    }
    //#MARK:- API Calling
    
    func getShipmentList()
    {
        var para = [String:AnyObject]()
        para["driverID"] = DriverId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.ShipmentList.removeAll()
            startRefreshTheView()
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.tbl_Shipment.reloadData()
            })
            GetShipmentListManager().getShipmentList(param: para) { [weak self] ShipmentListData in
                self!.stopRefreshTheView()
                guard ShipmentListData == nil else{
                    ShipmentListData!.forEach { Shipment in
                        self!.ShipmentList.append(Shipment)
                        let FromCoordinatesArr = Shipment.fromCoordinates!.components(separatedBy: ":")
                        print(Shipment.id!)
                        
                        
                        if UserDefaults.standard.value(forKey: String(Shipment.id!)) == nil {
                            
                            UserDefaults.standard.set("99", forKey: String(Shipment.id!))
                            UserDefaults.standard.synchronize()
                        }
                        
                            
                        self!.setMonitoredRegion(lati: Double(FromCoordinatesArr[0])!, longi: Double(FromCoordinatesArr[1])!, id: Shipment.controlNumber!, FromTo: "From_",ShipmentId: Shipment.id!)
                        let ToCoordinatesArr = Shipment.toCoordinates!.components(separatedBy: ":")
                        self!.setMonitoredRegion(lati: Double(ToCoordinatesArr[0])!, longi: Double(ToCoordinatesArr[1])!, id: Shipment.controlNumber!, FromTo: "To_",ShipmentId: Shipment.id!)
                        
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                        self!.tbl_Shipment.reloadData()
                    })
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self!.tbl_Shipment.reloadData()
                })
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
    func getSearchShipmentList()
    {
        var para = [String:AnyObject]()
        para["driverID"] = DriverId as AnyObject
        para["searchTerm"] = txtSearch.text! as AnyObject
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.ShipmentList.removeAll()
            startRefreshTheView()
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.tbl_Shipment.reloadData()
            })
            GetShipmentListManager().getSearchShipmentList(param: para) { [weak self] ShipmentListData in
                self!.stopRefreshTheView()
                guard ShipmentListData == nil else{
                    ShipmentListData!.forEach { Shipment in
                        self!.ShipmentList.append(Shipment)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                        self!.tbl_Shipment.reloadData()
                    })
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self!.tbl_Shipment.reloadData()
                })
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }
    
}
extension HomeViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if self.ShipmentList.count > 0 {
            self.tbl_Shipment.backgroundView = nil
            numOfSection = 1
            
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbl_Shipment.bounds.width, height: self.tbl_Shipment.bounds.height))
            noDataLabel.text = mapping.string(forKey: "NoShipmentListFound_key")
            noDataLabel.textColor = kDefaultTextColor
            noDataLabel.numberOfLines = 4
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tbl_Shipment.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.ShipmentList.count > 0){
            return self.ShipmentList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeVCTableViewCell") as! HomeVCTableViewCell
        cell.tag = indexPath.row
        print(self.ShipmentList.count)
        print(indexPath.row)
        if(self.ShipmentList.count > 0){
            cell.setTheData(theModel: self.ShipmentList[indexPath.row])
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let theModel = self.ShipmentList[indexPath.row]
        let itemDetails = self.storyboard!.instantiateViewController(withIdentifier: "ItemDetailsViewController") as! ItemDetailsViewController
        itemDetails.itemId = theModel.id!
        itemDetails.freightShipment = theModel.freightShipment!
        itemDetails.controlNumber = theModel.clientCN!
        itemDetails.freightShipmentBool = theModel.freightShipment!
        self.navigationController?.pushViewController(itemDetails, animated: true)
    }
}
extension HomeViewController: UITextFieldDelegate
{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text == "") {
            getShipmentList()
        }else{
            getSearchShipmentList()
        }
    }
}

