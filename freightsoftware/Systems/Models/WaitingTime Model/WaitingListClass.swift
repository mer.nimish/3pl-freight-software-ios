//
//  WaitingListClass.swift
//
//  Created by AB on 23/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class WaitingListClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pickupTo = "pickupTo"
    static let deliveryFrom = "deliveryFrom"
    static let pickupFrom = "pickupFrom"
    static let returnTo = "returnTo"
    static let deliveryTo = "deliveryTo"
    static let returnFrom = "returnFrom"
    
  }

  // MARK: Properties
  public var pickupFrom: String?
  public var pickupTo: String?
  public var deliveryFrom: String?
  public var deliveryTo: String?
  public var returnTo: String?
  public var returnFrom: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    pickupTo <- map[SerializationKeys.pickupTo]
    deliveryFrom <- map[SerializationKeys.deliveryFrom]
    pickupFrom <- map[SerializationKeys.pickupFrom]
    returnTo <- map[SerializationKeys.returnTo]
    deliveryTo <- map[SerializationKeys.deliveryTo]
    returnFrom <- map[SerializationKeys.returnFrom]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = pickupTo { dictionary[SerializationKeys.pickupTo] = value }
    if let value = deliveryFrom { dictionary[SerializationKeys.deliveryFrom] = value }
    if let value = pickupFrom { dictionary[SerializationKeys.pickupFrom] = value }
     if let value = returnTo { dictionary[SerializationKeys.returnTo] = value }
     if let value = deliveryTo { dictionary[SerializationKeys.deliveryTo] = value }
     if let value = returnFrom { dictionary[SerializationKeys.returnFrom] = value }
    return dictionary
  }

}
