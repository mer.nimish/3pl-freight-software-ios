//
//  ShipmentListClass.swift
//
//  Created by AB on 17/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ShipmentListClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let shipmentLeg = "shipmentLeg"
    static let clientCN = "clientCN"
    static let freightShipment = "freightShipment"
    static let id = "id"
    static let fromCoordinates = "fromCoordinates"
    static let controlNumber = "controlNumber"
    static let shipmentType = "shipmentType"
    static let moveType = "moveType"
    static let freightDetails = "freightDetails"
    static let priority = "priority"
    static let toCoordinates = "toCoordinates"
    static let stopNumber = "stopNumber"
  }

  // MARK: Properties
  public var shipmentLeg: Int?
  public var clientCN: Int?
  public var freightShipment: Bool? = false
  public var id: Int?
  public var fromCoordinates: String?
  public var controlNumber: Int?
  public var shipmentType: String?
  public var moveType: String?
  public var freightDetails: String?
  public var priority: Int?
  public var toCoordinates: String?
  public var stopNumber: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    shipmentLeg <- map[SerializationKeys.shipmentLeg]
    clientCN <- map[SerializationKeys.clientCN]
    freightShipment <- map[SerializationKeys.freightShipment]
    id <- map[SerializationKeys.id]
    fromCoordinates <- map[SerializationKeys.fromCoordinates]
    controlNumber <- map[SerializationKeys.controlNumber]
    shipmentType <- map[SerializationKeys.shipmentType]
    moveType <- map[SerializationKeys.moveType]
    freightDetails <- map[SerializationKeys.freightDetails]
    priority <- map[SerializationKeys.priority]
    toCoordinates <- map[SerializationKeys.toCoordinates]
    stopNumber <- map[SerializationKeys.stopNumber]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = shipmentLeg { dictionary[SerializationKeys.shipmentLeg] = value }
    if let value = clientCN { dictionary[SerializationKeys.clientCN] = value }
    dictionary[SerializationKeys.freightShipment] = freightShipment
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = fromCoordinates { dictionary[SerializationKeys.fromCoordinates] = value }
    if let value = controlNumber { dictionary[SerializationKeys.controlNumber] = value }
    if let value = shipmentType { dictionary[SerializationKeys.shipmentType] = value }
    if let value = moveType { dictionary[SerializationKeys.moveType] = value }
    if let value = freightDetails { dictionary[SerializationKeys.freightDetails] = value }
    if let value = priority { dictionary[SerializationKeys.priority] = value }
    if let value = toCoordinates { dictionary[SerializationKeys.toCoordinates] = value }
    if let value = stopNumber { dictionary[SerializationKeys.stopNumber] = value }
    return dictionary
  }

}
