//
//  LoginResponceClass.swift
//
//  Created by AB on 16/04/18
//  Copyright (c) Dignizant. All rights reserved.
//

import Foundation
import ObjectMapper

public final class LoginResponceClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let companyID = "company_ID"
    static let userName = "userName"
    static let driverID = "driverID"
    static let userID = "userID"
  }

  // MARK: Properties
  public var companyID: String?
  public var userName: String?
  public var driverID: Int?
  public var userID: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    companyID <- map[SerializationKeys.companyID]
    userName <- map[SerializationKeys.userName]
    driverID <- map[SerializationKeys.driverID]
    userID <- map[SerializationKeys.userID]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = companyID { dictionary[SerializationKeys.companyID] = value }
    if let value = userName { dictionary[SerializationKeys.userName] = value }
    if let value = driverID { dictionary[SerializationKeys.driverID] = value }
    if let value = userID { dictionary[SerializationKeys.userID] = value }
    return dictionary
  }

}
