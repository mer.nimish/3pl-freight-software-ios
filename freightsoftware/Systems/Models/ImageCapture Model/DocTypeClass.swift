//
//  DocTypeClass.swift
//
//  Created by AB on 19/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DocTypeClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iD = "ID"
    static let docName = "docName"
  }

  // MARK: Properties
  public var iD: Int?
  public var docName: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    iD <- map[SerializationKeys.iD]
    docName <- map[SerializationKeys.docName]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = docName { dictionary[SerializationKeys.docName] = value }
    return dictionary
  }

}
