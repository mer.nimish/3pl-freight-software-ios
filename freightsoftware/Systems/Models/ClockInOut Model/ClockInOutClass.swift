//
//  ClockInOutClass.swift
//
//  Created by AB on 17/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ClockInOutClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let clockOut = "clockOut"
    static let clockIn = "clockIn"
  }

  // MARK: Properties
  public var clockOut: String?
  public var clockIn: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    clockOut <- map[SerializationKeys.clockOut]
    clockIn <- map[SerializationKeys.clockIn]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = clockOut { dictionary[SerializationKeys.clockOut] = value }
    if let value = clockIn { dictionary[SerializationKeys.clockIn] = value }
    return dictionary
  }

}
