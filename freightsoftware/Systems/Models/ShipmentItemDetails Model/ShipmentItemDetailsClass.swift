//
//  ShipmentItemDetailsClass.swift
//
//  Created by AB on 18/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ShipmentItemDetailsClass: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    
    private struct SerializationKeys {
        static let toTime = "toTime"
        static let shipmentInstructions = "shipmentInstructions"
        static let terminalAppointmentNum = "terminalAppointmentNum"
        static let toAddress = "toAddress"
        static let transactionType = "transactionType"
        static let moveType = "moveType"
        static let stopNumber = "stopNumber"
        static let shipperRef = "shipperRef"
        static let tblFPShipmentDriverID = "tblFPShipmentDriverID"
        static let clientCN = "clientCN"
        static let sealNumber = "sealNumber"
        static let bookingNumber = "bookingNumber"
        static let controlNumber = "controlNumber"
        static let containerNumber = "containerNumber"
        static let toDate = "toDate"
        static let pin = "pin"
        static let fromAddress = "fromAddress"
        static let fromAddresss = "fromAddresss"
        static let moveTypeID = "moveTypeID"
        static let toName = "toName"
        static let steamShipline = "steamShipline"
        static let consigeeRef = "consigeeRef"
        static let railLocation = "railLocation"
        static let fromCoordinates = "fromCoordinates"
        static let freightDetails = "freightDetails"
        static let railReleaseNumber = "railReleaseNumber"
        static let fromName = "fromName"
        static let toCoordinates = "toCoordinates"
        static let terminalApptDate = "terminalApptDate"
        static let customerApptTime = "customerApptTime"
        static let customerAppt = "customerAppt"
        static let terminalAppTimes = "terminalAppTimes"
        static let terminalApptNumber = "terminalApptNumber"
        static let fromDate = "fromDate"
        static let fromTime = "fromTime"
        static let fromRef = "fromRef"
        static let toRef = "toRef"
        
    }
    
    // MARK: Properties
    public var toTime: String?
    public var shipmentInstructions: String?
    public var terminalAppointmentNum: String?
    public var toAddress: String?
    public var transactionType: String?
    public var moveType: String?
    public var stopNumber: String?
    public var shipperRef: String?
    public var tblFPShipmentDriverID: Int?
    public var clientCN: Int?
    public var sealNumber: String?
    public var bookingNumber: String?
    public var controlNumber: Int?
    public var containerNumber: String?
    public var toDate: String?
    public var pin: String?
    public var fromAddress: String?
    public var fromAddresss: String?
    public var moveTypeID: String?
    public var toName: String?
    public var steamShipline: String?
    public var consigeeRef: String?
    public var railLocation: String?
    public var fromCoordinates: String?
    public var freightDetails: String?
    public var railReleaseNumber: String?
    public var fromName: String?
    public var toCoordinates: String?
    public var fromDate: String?
    public var fromTime: String?
    public var fromRef: String?
    public var toRef: String?
    
    
    
    public var terminalApptDate: String?
    public var customerApptTime: String?
    public var customerAppt: String?
    public var terminalAppTimes: String?
    public var terminalApptNumber: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        toTime <- map[SerializationKeys.toTime]
        shipmentInstructions <- map[SerializationKeys.shipmentInstructions]
        terminalAppointmentNum <- map[SerializationKeys.terminalAppointmentNum]
        toAddress <- map[SerializationKeys.toAddress]
        transactionType <- map[SerializationKeys.transactionType]
        moveType <- map[SerializationKeys.moveType]
        stopNumber <- map[SerializationKeys.stopNumber]
        shipperRef <- map[SerializationKeys.shipperRef]
        tblFPShipmentDriverID <- map[SerializationKeys.tblFPShipmentDriverID]
        clientCN <- map[SerializationKeys.clientCN]
        sealNumber <- map[SerializationKeys.sealNumber]
        bookingNumber <- map[SerializationKeys.bookingNumber]
        controlNumber <- map[SerializationKeys.controlNumber]
        containerNumber <- map[SerializationKeys.containerNumber]
        toDate <- map[SerializationKeys.toDate]
        pin <- map[SerializationKeys.pin]
        fromAddress <- map[SerializationKeys.fromAddress]
        fromAddresss <- map[SerializationKeys.fromAddresss]
        moveTypeID <- map[SerializationKeys.moveTypeID]
        toName <- map[SerializationKeys.toName]
        steamShipline <- map[SerializationKeys.steamShipline]
        consigeeRef <- map[SerializationKeys.consigeeRef]
        railLocation <- map[SerializationKeys.railLocation]
        fromCoordinates <- map[SerializationKeys.fromCoordinates]
        freightDetails <- map[SerializationKeys.freightDetails]
        railReleaseNumber <- map[SerializationKeys.railReleaseNumber]
        fromName <- map[SerializationKeys.fromName]
        toCoordinates <- map[SerializationKeys.toCoordinates]
        terminalApptDate <- map[SerializationKeys.terminalApptDate]
        customerApptTime <- map[SerializationKeys.customerApptTime]
        customerAppt <- map[SerializationKeys.customerAppt]
        terminalAppTimes <- map[SerializationKeys.terminalAppTimes]
        fromDate <- map[SerializationKeys.fromDate]
        fromTime <- map[SerializationKeys.fromTime]
        fromRef <- map[SerializationKeys.fromRef]
        toRef <- map[SerializationKeys.toRef]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = toTime { dictionary[SerializationKeys.toTime] = value }
        if let value = shipmentInstructions { dictionary[SerializationKeys.shipmentInstructions] = value }
        if let value = terminalAppointmentNum { dictionary[SerializationKeys.terminalAppointmentNum] = value }
        if let value = toAddress { dictionary[SerializationKeys.toAddress] = value }
        if let value = transactionType { dictionary[SerializationKeys.transactionType] = value }
        if let value = moveType { dictionary[SerializationKeys.moveType] = value }
        if let value = stopNumber { dictionary[SerializationKeys.stopNumber] = value }
        if let value = shipperRef { dictionary[SerializationKeys.shipperRef] = value }
        if let value = tblFPShipmentDriverID { dictionary[SerializationKeys.tblFPShipmentDriverID] = value }
        if let value = clientCN { dictionary[SerializationKeys.clientCN] = value }
        if let value = sealNumber { dictionary[SerializationKeys.sealNumber] = value }
        if let value = bookingNumber { dictionary[SerializationKeys.bookingNumber] = value }
        if let value = controlNumber { dictionary[SerializationKeys.controlNumber] = value }
        if let value = containerNumber { dictionary[SerializationKeys.containerNumber] = value }
        if let value = toDate { dictionary[SerializationKeys.toDate] = value }
        if let value = pin { dictionary[SerializationKeys.pin] = value }
        if let value = fromAddresss { dictionary[SerializationKeys.fromAddresss] = value }
        if let value = moveTypeID { dictionary[SerializationKeys.moveTypeID] = value }
        if let value = toName { dictionary[SerializationKeys.toName] = value }
        if let value = steamShipline { dictionary[SerializationKeys.steamShipline] = value }
        if let value = consigeeRef { dictionary[SerializationKeys.consigeeRef] = value }
        if let value = railLocation { dictionary[SerializationKeys.railLocation] = value }
        if let value = fromCoordinates { dictionary[SerializationKeys.fromCoordinates] = value }
        if let value = freightDetails { dictionary[SerializationKeys.freightDetails] = value }
        if let value = railReleaseNumber { dictionary[SerializationKeys.railReleaseNumber] = value }
        if let value = fromName { dictionary[SerializationKeys.fromName] = value }
        if let value = toCoordinates { dictionary[SerializationKeys.toCoordinates] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.toTime = aDecoder.decodeObject(forKey: SerializationKeys.toTime) as? String
        self.shipmentInstructions = aDecoder.decodeObject(forKey: SerializationKeys.shipmentInstructions) as? String
        self.terminalAppointmentNum = aDecoder.decodeObject(forKey: SerializationKeys.terminalAppointmentNum) as? String
        self.toAddress = aDecoder.decodeObject(forKey: SerializationKeys.toAddress) as? String
        self.transactionType = aDecoder.decodeObject(forKey: SerializationKeys.transactionType) as? String
        self.moveType = aDecoder.decodeObject(forKey: SerializationKeys.moveType) as? String
        self.stopNumber = aDecoder.decodeObject(forKey: SerializationKeys.stopNumber) as? String
        self.shipperRef = aDecoder.decodeObject(forKey: SerializationKeys.shipperRef) as? String
        self.tblFPShipmentDriverID = aDecoder.decodeObject(forKey: SerializationKeys.tblFPShipmentDriverID) as? Int
        self.clientCN = aDecoder.decodeObject(forKey: SerializationKeys.clientCN) as? Int
        self.sealNumber = aDecoder.decodeObject(forKey: SerializationKeys.sealNumber) as? String
        self.bookingNumber = aDecoder.decodeObject(forKey: SerializationKeys.bookingNumber) as? String
        self.controlNumber = aDecoder.decodeObject(forKey: SerializationKeys.controlNumber) as? Int
        self.containerNumber = aDecoder.decodeObject(forKey: SerializationKeys.containerNumber) as? String
        self.toDate = aDecoder.decodeObject(forKey: SerializationKeys.toDate) as? String
        self.pin = aDecoder.decodeObject(forKey: SerializationKeys.pin) as? String
        self.fromAddresss = aDecoder.decodeObject(forKey: SerializationKeys.fromAddresss) as? String
        self.moveTypeID = aDecoder.decodeObject(forKey: SerializationKeys.moveTypeID) as? String
        self.toName = aDecoder.decodeObject(forKey: SerializationKeys.toName) as? String
        self.steamShipline = aDecoder.decodeObject(forKey: SerializationKeys.steamShipline) as? String
        self.consigeeRef = aDecoder.decodeObject(forKey: SerializationKeys.consigeeRef) as? String
        self.railLocation = aDecoder.decodeObject(forKey: SerializationKeys.railLocation) as? String
        self.fromCoordinates = aDecoder.decodeObject(forKey: SerializationKeys.fromCoordinates) as? String
        self.freightDetails = aDecoder.decodeObject(forKey: SerializationKeys.freightDetails) as? String
        self.railReleaseNumber = aDecoder.decodeObject(forKey: SerializationKeys.railReleaseNumber) as? String
        self.fromName = aDecoder.decodeObject(forKey: SerializationKeys.fromName) as? String
        self.toCoordinates = aDecoder.decodeObject(forKey: SerializationKeys.toCoordinates) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(toTime, forKey: SerializationKeys.toTime)
        aCoder.encode(shipmentInstructions, forKey: SerializationKeys.shipmentInstructions)
        aCoder.encode(terminalAppointmentNum, forKey: SerializationKeys.terminalAppointmentNum)
        aCoder.encode(toAddress, forKey: SerializationKeys.toAddress)
        aCoder.encode(transactionType, forKey: SerializationKeys.transactionType)
        aCoder.encode(moveType, forKey: SerializationKeys.moveType)
        aCoder.encode(stopNumber, forKey: SerializationKeys.stopNumber)
        aCoder.encode(shipperRef, forKey: SerializationKeys.shipperRef)
        aCoder.encode(tblFPShipmentDriverID, forKey: SerializationKeys.tblFPShipmentDriverID)
        aCoder.encode(clientCN, forKey: SerializationKeys.clientCN)
        aCoder.encode(sealNumber, forKey: SerializationKeys.sealNumber)
        aCoder.encode(bookingNumber, forKey: SerializationKeys.bookingNumber)
        aCoder.encode(controlNumber, forKey: SerializationKeys.controlNumber)
        aCoder.encode(containerNumber, forKey: SerializationKeys.containerNumber)
        aCoder.encode(toDate, forKey: SerializationKeys.toDate)
        aCoder.encode(pin, forKey: SerializationKeys.pin)
        aCoder.encode(fromAddresss, forKey: SerializationKeys.fromAddresss)
        aCoder.encode(moveTypeID, forKey: SerializationKeys.moveTypeID)
        aCoder.encode(toName, forKey: SerializationKeys.toName)
        aCoder.encode(steamShipline, forKey: SerializationKeys.steamShipline)
        aCoder.encode(consigeeRef, forKey: SerializationKeys.consigeeRef)
        aCoder.encode(railLocation, forKey: SerializationKeys.railLocation)
        aCoder.encode(fromCoordinates, forKey: SerializationKeys.fromCoordinates)
        aCoder.encode(freightDetails, forKey: SerializationKeys.freightDetails)
        aCoder.encode(railReleaseNumber, forKey: SerializationKeys.railReleaseNumber)
        aCoder.encode(fromName, forKey: SerializationKeys.fromName)
        aCoder.encode(toCoordinates, forKey: SerializationKeys.toCoordinates)
    }
    
}

