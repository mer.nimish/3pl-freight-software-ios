//
//  SelectEmptyContainerClass.swift
//
//  Created by AB on 24/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class SelectEmptyContainerClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let containerNumber = "containerNumber"
    static let terminal = "terminal"
    static let chassisNumber = "chassisNumber"
  }

  // MARK: Properties
  public var containerNumber: String?
  public var terminal: String?
  public var chassisNumber: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    containerNumber <- map[SerializationKeys.containerNumber]
    terminal <- map[SerializationKeys.terminal]
    chassisNumber <- map[SerializationKeys.chassisNumber]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = containerNumber { dictionary[SerializationKeys.containerNumber] = value }
    if let value = terminal { dictionary[SerializationKeys.terminal] = value }
    if let value = chassisNumber { dictionary[SerializationKeys.chassisNumber] = value }
    return dictionary
  }

}
