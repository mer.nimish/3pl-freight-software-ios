//
//  FreightDetailsDimListClass.swift
//
//  Created by AB on 21/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class FreightDetailsDimListClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let piecesType = "piecesType"
    static let w = "w"
    static let id = "id"
    static let dimFactor = "dimFactor"
    static let weight = "weight"
    static let quantity = "quantity"
    static let l = "l"
    static let pieceDescription = "pieceDescription"
    static let h = "h"
  }

  // MARK: Properties
  public var piecesType: String?
  public var w: Int?
  public var id: Int?
  public var dimFactor: Int?
  public var weight: Int?
  public var quantity: Int?
  public var l: Int?
  public var pieceDescription: String?
  public var h: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    piecesType <- map[SerializationKeys.piecesType]
    w <- map[SerializationKeys.w]
    id <- map[SerializationKeys.id]
    dimFactor <- map[SerializationKeys.dimFactor]
    weight <- map[SerializationKeys.weight]
    quantity <- map[SerializationKeys.quantity]
    l <- map[SerializationKeys.l]
    pieceDescription <- map[SerializationKeys.pieceDescription]
    h <- map[SerializationKeys.h]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = piecesType { dictionary[SerializationKeys.piecesType] = value }
    if let value = w { dictionary[SerializationKeys.w] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = dimFactor { dictionary[SerializationKeys.dimFactor] = value }
    if let value = weight { dictionary[SerializationKeys.weight] = value }
    if let value = quantity { dictionary[SerializationKeys.quantity] = value }
    if let value = l { dictionary[SerializationKeys.l] = value }
    if let value = pieceDescription { dictionary[SerializationKeys.pieceDescription] = value }
    if let value = h { dictionary[SerializationKeys.h] = value }
    return dictionary
  }

}
