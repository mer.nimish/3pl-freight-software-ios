//
//  ShimpentNotesListClass.swift
//
//  Created by AB on 20/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ShimpentNotesListClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let noteType = "noteType"
  }

  // MARK: Properties
  public var id: Int?
  public var noteType: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    noteType <- map[SerializationKeys.noteType]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = noteType { dictionary[SerializationKeys.noteType] = value }
    return dictionary
  }

}
