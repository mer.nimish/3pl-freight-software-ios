//
//  AppDelegate.swift
//  freightsoftware
//
//  Created by DK on 14/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import UserNotifications
import NotificationCenter
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SWRevealViewControllerDelegate {

    var window: UIWindow?
    var locationManager = CLLocationManager()
    var DriverLat:Double = 0.0
    var DriverLong:Double = 0.0
    var timerLat:Timer?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        ScrenHeight = UIScreen.main.bounds.height
        ScrenWidth = UIScreen.main.bounds.width
        checkUserIsLoginOrNot()
        //TODO: - Language Chnage
        setLangauge(language: English)
        
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 10
        self.locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
        if CLLocationManager.locationServicesEnabled()
        {
            switch(CLLocationManager.authorizationStatus())
            {
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("Authorize.")
                 locationManager.startUpdatingLocation()
                
                break
                
            case .notDetermined:
                
                print("Not determined.")
                locationManager.requestWhenInUseAuthorization()
                
                break
                
            case .restricted:
                
                print("Restricted.")
                let alertController = UIAlertController(title: "Oops !!" , message: "Location service seems to be restricted. Please enable from Settings -> Privacy ->LocationService.", preferredStyle: .alert)
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertController.addAction(cancelAction)
                self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                
                break
                
            case .denied:
                
                print("Denied.")
                let alertController = UIAlertController(title: "Oops !!" , message: "Location service seems to be denied. Please enable from Settings -> Privacy ->LocationService.", preferredStyle: .alert)
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertController.addAction(cancelAction)
               self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
            }
        }
        timerLat = Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: { (timer) in
            UserDefaults.standard.set(true, forKey: "sendLocation")
            UserDefaults.standard.synchronize()
            self.locationManager.startUpdatingLocation()
        })
        
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
        UIApplication.shared.cancelAllLocalNotifications()
            
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        
        
    }
    //#MARK:- Member function
  
    func checkUserIsLoginOrNot()
    {
        if let isKeepMeLogin:Bool = userdefualts.object(forKey: "isKeepMeLogin") as? Bool {
            if isKeepMeLogin
            {
                print(userdefualts.value(forKey: "isKeepMeEmail") ?? "")
                print(userdefualts.value(forKey: "isKeepMePassword") ?? "")
                if let LoginData = userdefualts.object(forKey: "LoginData") as? [String:AnyObject] {
                    print(LoginData)
                    print(LoginData["DriverUserName"] as! String)
                    CompanyId =  LoginData["CompanyId"] as! String
                    DriverUserId = LoginData["DriverUserId"] as! Int
                    DriverId = LoginData["DriverId"] as! Int
                    DriverUserName = LoginData["DriverUserName"] as! String
                    UserIsLogin = true
                    
                    /*let testController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                     let appDelegate = UIApplication.shared.delegate as! AppDelegate
                     appDelegate.window?.rootViewController = testController*/
                    
                }else{
                    UserIsLogin = false
                }
            }else{
                if let LoginData = userdefualts.object(forKey: "LoginData") as? [String:AnyObject] {
                    print(LoginData)
                    print(LoginData["DriverUserName"] as! String)
                    CompanyId =  LoginData["CompanyId"] as! String
                    DriverUserId = LoginData["DriverUserId"] as! Int
                    DriverId = LoginData["DriverId"] as! Int
                    DriverUserName = LoginData["DriverUserName"] as! String
                    UserIsLogin = true
                }else{
                    UserIsLogin = false
                }
            }
        }else{
            if let LoginData = userdefualts.object(forKey: "LoginData") as? [String:AnyObject] {
                print(LoginData)
                print(LoginData["DriverUserName"] as! String)
                CompanyId =  LoginData["CompanyId"] as! String
                DriverUserId = LoginData["DriverUserId"] as! Int
                DriverId = LoginData["DriverId"] as! Int
                DriverUserName = LoginData["DriverUserName"] as! String
                UserIsLogin = true
            }else{
                UserIsLogin = false
            }
        }
        print(UserIsLogin)
        if UserIsLogin == true
        {
            let frontViewController = MainStoryBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
            let rearViewController = MainStoryBoard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
            
            let frontNavigationController = UINavigationController(rootViewController: frontViewController)
            frontNavigationController.navigationBar.isTranslucent = false
            let rearNavigationController = UINavigationController(rootViewController: rearViewController)
            rearNavigationController.navigationBar.isTranslucent = false
            let revealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController)
            //revealController?.panGestureRecognizer().isEnabled = true
            //revealController?.tapGestureRecognizer().isEnabled = true
            revealController?.navigationController?.isNavigationBarHidden = false
            revealController?.rearViewRevealWidth = (frontViewController.view.frame.size.width) - 75
            
            revealController?.delegate = self
            
            let window = UIApplication.shared.delegate?.window as? UIWindow
            
            window?.rootViewController = revealController
            window?.makeKeyAndVisible()
        }
        
    }
    func handleEvent(forRegion region: CLRegion!,msg:String) {
        // Show an alert if application is active
        if UIApplication.shared.applicationState == .active {
            let message = msg
            window?.rootViewController?.showAlert(message: message)
        } else {
            // Otherwise present a local notification
            print(msg)
            let notification = UILocalNotification()
            notification.alertTitle = mapping.string(forKey: "freightsoftware_key")
            notification.alertBody = msg
            notification.soundName = "Default"
            UIApplication.shared.presentLocalNotificationNow(notification)
        }
    }


}
// MARK: - Location Manager Delegate
extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        /*for currentLocation in locations
        {
            print("\(index):\(currentLocation)")
            print("Long \(currentLocation.coordinate.longitude)")
            print("Lati \(currentLocation.coordinate.latitude)")
        }*/
        let lastLocation = locations.last!
        if(DriverLat != lastLocation.coordinate.longitude && DriverLong != lastLocation.coordinate.latitude)
        {
            DriverLat = lastLocation.coordinate.latitude
            DriverLong = lastLocation.coordinate.longitude
        }
        
        
        var counter = 0 
        if UserDefaults.standard.object(forKey: "LocationCounter") != nil {
            counter = UserDefaults.standard.object(forKey: "LocationCounter") as! Int
        }
        
         if UserDefaults.standard.bool(forKey: "sendLocation") == true {
            
            UserDefaults.standard.set(counter+1, forKey: "LocationCounter")
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(false, forKey: "sendLocation")
            UserDefaults.standard.synchronize()
            if(DriverId != 0 && DriverLat != 0.0 && DriverLong != 0.0)
            {
                UpdateDriverLocation()
            }
        }
        
        locationManager.stopUpdatingLocation()
        
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //mapView.showsUserLocation = status == .authorizedAlways
        print("status-->\(status)")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("Monitoring with identifier: \(region.identifier)")
        print("The monitored regions are: \(manager.monitoredRegions)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        NSLog("Entered with identifier: \(region.identifier)")
        
        print(region.identifier)
        let regionId = region.identifier
        let regions = regionId.components(separatedBy: "-")
        let ShipmentId = regions[0]
        print(ShipmentId)
        let Logs = regions[1].components(separatedBy: "_")
        
        
        if(Logs[0] == "To")
        {
            
            if (UserDefaults.standard.value(forKey: ShipmentId) != nil)
            {
                if UserDefaults.standard.value(forKey: ShipmentId) as! String  == "117"
                {
                    UserDefaults.standard.setValue("118", forKey: ShipmentId)
                    UserDefaults.standard.synchronize()
                    
                    if(DriverUserId != 0 && DriverUserName != "")
                    {
                        UpdateShipmentLog(contollNumber:Logs[1],ContainerLogId:118)
                        SaveDriverGeoFence(ShipmentId:ShipmentId,logType:2)
                    }
                    
                    self.handleEvent(forRegion: region, msg: mapping.string(forKey: "3PLFreightSoftwareDispatchArrival"))
                }
            }
            
        }else{
            
            if (UserDefaults.standard.value(forKey: ShipmentId) != nil)
            {
                if UserDefaults.standard.value(forKey: ShipmentId) as! String  == "99"
                {
                    UserDefaults.standard.setValue("116", forKey: ShipmentId)
                    UserDefaults.standard.synchronize()
                    
                    if(DriverUserId != 0 && DriverUserName != "")
                    {
                        UpdateShipmentLog(contollNumber:Logs[1],ContainerLogId:116)
                        SaveDriverGeoFence(ShipmentId:ShipmentId,logType:0)
                    }
                    
                    self.handleEvent(forRegion: region, msg: mapping.string(forKey: "3PLFreightSoftwareDispatchArrival"))
                }
                
            }
            
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        //print("Bye! Hope you had a great day at the beach!-->\(region.identifier)")
        //Good place to schedule a local notification
        NSLog("Exited with identifier: \(region.identifier)")
        
        print(region.identifier)
        let regionId = region.identifier
        let regions = regionId.components(separatedBy: "-")
        let ShipmentId = regions[0]
        print(ShipmentId)
        let Logs = regions[1].components(separatedBy: "_")
        if(Logs[0] == "To")
        {
            if (UserDefaults.standard.value(forKey: ShipmentId) != nil)
            {
                if UserDefaults.standard.value(forKey: ShipmentId) as! String  == "118"
                {
                    UserDefaults.standard.setValue("119", forKey: ShipmentId)
                    UserDefaults.standard.synchronize()
                    
                    if(DriverUserId != 0 && DriverUserName != "")
                    {
                        UpdateShipmentLog(contollNumber:Logs[1],ContainerLogId:119)
                        SaveDriverGeoFence(ShipmentId:ShipmentId,logType:3)
                        locationManager.stopMonitoring(for: region)
                        
                        //Start location manager and fetch current location
                        locationManager.startUpdatingLocation()
                        
                        
                    }
                    self.handleEvent(forRegion: region, msg: mapping.string(forKey: "3PLFreightSoftwareDeparture"))
                }
            }
            
            
        }else{
            if (UserDefaults.standard.value(forKey: ShipmentId) != nil)
            {
                if UserDefaults.standard.value(forKey: ShipmentId) as! String  == "116"
                {
                    UserDefaults.standard.setValue("117", forKey: ShipmentId)
                    UserDefaults.standard.synchronize()
                    
                    if(DriverUserId != 0 && DriverUserName != "")
                    {
                        UpdateShipmentLog(contollNumber:Logs[1],ContainerLogId:117)
                        SaveDriverGeoFence(ShipmentId:ShipmentId,logType:1)
                        
                        locationManager.stopMonitoring(for: region)
                        
                        //Start location manager and fetch current location
                        locationManager.startUpdatingLocation()
                        
                        
                    }
                    self.handleEvent(forRegion: region, msg: mapping.string(forKey: "3PLFreightSoftwareDeparture"))
                }
            }
            
        }
        
    }
    func UpdateDriverLocation()
    {
        var para = [String:AnyObject]()
        para["driverID"] = DriverId as AnyObject
        para["latitude"] = DriverLat as AnyObject
        para["longitude"] = DriverLong as AnyObject
        
        print(para)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetCaptureImageOutManager().postUpdateDriverLocation(param: para) { [weak self] DriverLocationData in
                
                guard DriverLocationData == nil else{
                    print("Driver location update successfully")
                    return
                }
            }
        }
        else
        {
            //makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func UpdateShipmentLog(contollNumber:String,ContainerLogId:Int)
    {
        var para = [String:AnyObject]()
        para["controlNumber"] = contollNumber as AnyObject
        para["userID"] = DriverUserId as AnyObject
        para["userName"] = DriverUserName as AnyObject
        para["logID"] = ContainerLogId as AnyObject
        print(para)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetCaptureImageOutManager().postUpdateShipmentLog(param: para) { [weak self] DriverLocationData in
                
                guard DriverLocationData == nil else{
                    print("ShipmentLog update successfully")
                    
                    return
                }
            }
        }
        else
        {
            //makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func SaveDriverGeoFence(ShipmentId:String,logType:Int)
    {
        var para = [String:AnyObject]()
        para["id"] = ShipmentId as AnyObject
        para["logType"] = logType as AnyObject
        print(para)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetCaptureImageOutManager().postsSaveDriverGeoFence(param: para) { [weak self] DriverLocationData in
                
                guard DriverLocationData == nil else{
                    print("SaveDriverGeoFence update successfully")
                    self?.window?.rootViewController?.showAlert(message: "SaveDriverGeoFence update successfully")
                    return
                }
            }
        }
        else
        {
            //makeToast(strMessage: NointernetConnection!)
        }
    }
    
}

